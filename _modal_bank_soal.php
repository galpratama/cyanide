<!-- Modal -->
<div class="modal fade" id="modalBankSoal" tabindex="-1" role="dialog" aria-labelledby="filterScoreLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-center" id="forgotPasswordLabel">Pilihan Soal</h4>
      </div>
      <div class="modal-body">
        <p class="text-center">
          Silahkan pilih soal-soal berikut yang ingin dimasukkan ke "Nama Ujian"
        </p>
        <div class="scrollable-table-list">
          <table class="table">
            <thead>
              <tr>
                <th width="5%">No.</th>
                <th width="75%">Pertanyaan</th>
                <th width="15%">Kesulitan</th>
                <th width="5%"><input type="checkbox"></th>
              </tr>
            </thead>
            <tbody>
                <?php
                for ($i=1; $i < 20; $i++) {
                  ?>
                    <tr>
                      <td width="5%"><?php echo $i;?></td>
                      <td width="75%">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit rerum voluptas tempore.</td>
                      <td width="15%"><span class="label label-success">Mudah</span></td>
                      <td width="5%">
                        <input type="checkbox">
                      </td>
                    </tr>
                  <?php
                }
                ?>
              </tbody>
            </table>
            <br><br>
            <button type="submit" class="btn btn-pn btn-pn-primary btn-lg btn-block">Tambahkan</button>
        </div>
      </div>
    </div>
  </div>
</div>
