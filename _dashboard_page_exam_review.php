<div class="container-fluid">
  <div class="row">
    <?php include '_breadcrumb_exam_review.php'; ?>
    <div class="col-lg-12">
      <h2>
        Review Jawaban Siswa
        <br>
        <small>Ujian Essay 1 Matematika</small>
      </h2>
      <blockquote>
        <strong>Soal:</strong> <br>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit rerum voluptas tempore officiis! Iste illo impedit, deserunt, a vero at laborum deleniti explicabo, harum veritatis quas temporibus molestiae sit dicta.?
      </blockquote>
      <div class="row">
        <?php include '_card_exam_review.php'; ?>
        <?php include '_card_exam_review.php'; ?>
        <?php include '_card_exam_review.php'; ?>
        <?php include '_card_exam_review.php'; ?>
        <?php include '_card_exam_review.php'; ?>
        <?php include '_card_exam_review.php'; ?>
        <div class="col-md-12">
          <div class="col-card">
            <div class="row">
              <div class="col-md-4">
                <button type="button" class="btn btn-danger btn-lg btn-pn-round btn-block next-step"><i class="fa fa-arrow-left"></i> Kembali</button>
              </div>
              <div class="col-md-8">
                <button type="button" class="btn btn-pn-primary btn-lg btn-pn-round btn-block next-step">Simpan <i class="fa fa-save"></i></button>
              </div>
            </div>
          </div>
        </div>
        </div><!-- /.col-md-8 -->
        </div><!-- /.row -->
      </div><!-- /.row -->
    </div><!-- /.col-md-12 -->
  </div><!-- /.row -->
</div><!-- /.container-fluid -->
<script type="text/javascript" src="libraries/canvasjs-1.8.0/canvasjs.min.js"></script>
<script type="text/javascript" src="scripts/column_chart.js"></script>