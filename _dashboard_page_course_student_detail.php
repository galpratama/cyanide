<div class="container-fluid">
  <div class="row">
    <?php include '_breadcrumb_course_student_detail.php'; ?>
    <div class="col-lg-12">
        <h3>
            Bahasa Korea XII MIA 1, Semester 2 <br><small><i class="fa fa-user-circle-o"></i> Pengajar oleh <strong>Galih Pratama</strong></small>
        </h3>
        <div class="row">
          <div class="col-md-2">
            <div class="row">
              <div class="col-md-12">
                <div class="col-card">
                  <ul class="nav nav-pills nav-stacked">
                    <li role="presentation" class="active"><a href="dashboard.php?page=course-student-detail"><i class="fa fa-file-pdf-o"></i> Materi</a></li>
                    <li role="presentation"><a href="dashboard.php?page=course-student-task"><i class="fa fa-drivers-license-o"></i> Tugas</a></li>
                    <li role="presentation"><a href="dashboard.php?page=course-student-exam"><i class="fa fa-check-circle"></i> Ujian</a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-10">
            <div class="row">
                <a href="dashboard.php?page=course-student-detail-video">
                    <div class="col-md-4">
                        <div class="col-card card-course-student row">
                            <div class="col-md-12">
                                <div class="course-cover-image">
                                    <div class="course-number">1</div>
                                    <div class="course-type">
                                        <i class="fa fa-play-circle-o"></i>
                                    </div>
                                    <img src="https://swanlake1701.files.wordpress.com/2013/12/korean-book-haul.jpg"
                                         alt="">
                                </div>
                                <div class="row">
                                    <div class="course-title">
                                        <h3>Pengenalan Pada Bahasa Korea</h3>
                                        <p>
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. A doloremque ducimus exercitationem fugiat, magni nam nostrum odit placeat quam quisquam?
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </a>
                <a href="dashboard.php?page=course-student-detail-attachment">
                    <div class="col-md-4">
                        <div class="col-card card-course-student row">
                            <div class="col-md-12">
                                <div class="course-cover-image">
                                    <div class="course-number">2</div>
                                    <div class="course-type">
                                        <i class="fa fa-file-pdf-o"></i>
                                    </div>
                                    <img src="https://koreanclassmassive.files.wordpress.com/2012/02/dirty-korean-language-book-3.jpg"
                                         alt="">
                                </div>
                                <div class="row">
                                    <div class="course-title">
                                        <h3>Cara Menulis Huruf Hangul Dengan Benar</h3>
                                        <p>
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. A doloremque ducimus exercitationem fugiat, magni nam nostrum odit placeat quam quisquam?
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </a>
                <a href="dashboard.php?page=course-student-detail-text">
                    <div class="col-md-4">
                        <div class="col-card card-course-student row">
                            <div class="col-md-12">
                                <div class="course-cover-image">
                                    <div class="course-number">3</div>
                                    <div class="course-type">
                                        <i class="fa fa-file-text-o"></i>
                                    </div>
                                    <img src="http://payload167.cargocollective.com/1/11/367988/5656167/IMG_0422.JPG"
                                         alt="">
                                </div>
                                <div class="row">
                                    <div class="course-title">
                                        <h3>Kosakata Yang Sering Dipakai Sehari-hari</h3>
                                        <p>
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. A doloremque ducimus exercitationem fugiat, magni nam nostrum odit placeat quam quisquam?
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </a>
                <a href="dashboard.php?page=course-student-detail-video">
                    <div class="col-md-4">
                        <div class="col-card card-course-student row">
                            <div class="col-md-12">
                                <div class="course-cover-image">
                                    <div class="course-number">4</div>
                                    <div class="course-type">
                                        <i class="fa fa-play-circle-o"></i>
                                    </div>
                                    <img src="https://vikiofficial.files.wordpress.com/2014/06/b89f6-languagelearningswag.jpg"
                                         alt="">
                                </div>
                                <div class="row">
                                    <div class="course-title">
                                        <h3>Perbedaan Bahasa Korea dengan Jepang</h3>
                                        <p>
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. A doloremque ducimus exercitationem fugiat, magni nam nostrum odit placeat quam quisquam?
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </a>
                <div class="col-md-4">
                    <a href="dashboard.php?page=course-student-detail-video">
                        <div class="col-card card-course-student row">
                            <div class="col-md-12">
                                <div class="course-cover-image">
                                    <div class="course-number">5</div>
                                    <div class="course-type">
                                        <i class="fa fa-play-circle-o"></i>
                                    </div>
                                    <img src="https://bluehanbok.files.wordpress.com/2015/04/3.png"
                                         alt="">
                                </div>
                                <div class="row">
                                    <div class="course-title">
                                        <h3>Tips dan Trik Belajar Bahasa Korea</h3>
                                        <p>
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. A doloremque ducimus exercitationem fugiat, magni nam nostrum odit placeat quam quisquam?
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="libraries/canvasjs-1.8.0/canvasjs.min.js"></script>
<script type="text/javascript" src="libraries/jquery/jquery.min.js"></script>
<script type="text/javascript" src="scripts/course_detail.js"></script>
