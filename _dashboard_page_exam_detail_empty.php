<div class="container-fluid">
  <div class="row">
    <?php include '_breadcrumb_exam_detail.php'; ?>
    <div class="col-lg-12">
      <h2>Ujian 1 Matematika</h2>
      <div class="row">
        <div class="col-md-8">
          <h4>Grafik Sebaran Nilai</h4>
          <div class="col-card">
            <div class="chart-column">
              <div id="chartContainer"
                   style="height: 323px; width: 100%;">
              </div>
              <div class="chart-empty">
                <p>
                  Grafik Sebaran Nilai Masih Kosong
                </p>
                <a href="#" class="btn btn-pn-primary btn-sm">
                  <i class="fa fa-plus-circle"></i> Tambah Soal
                </a>
              </div><!-- /.chart-empty -->
              <br />
              <div class="text-center">Sebaran Nilai</div><!-- /.text-center -->
            </div>
          </div><!-- /.col-card -->
        </div><!-- /.col-md-8 -->
        <div class="col-md-4">
          <h4>Informasi dan Status</h4>
          <?php include '_card_exam_information.php'; ?>
        </div><!-- /.col-md-4 -->
      </div><!-- /.row -->
      <div class="row">
        <div class="col-md-12">
          <h4>Detail Ujian</h4>
          <div class="col-card card-floating-button">
            <div>
              <!-- Nav tabs -->
              <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active">
                  <a href="#tabSoal" aria-controls="tabSoal" role="tab" data-toggle="tab"> 
                    <i class="fa fa-list"></i> Soal
                  </a>
                </li>
                <li role="presentation">
                  <a href="#tabAnalisis" aria-controls="tabAnalisis" role="tab" data-toggle="tab"><i class="fa fa-chart"></i>
                    <i class="fa fa-line-chart"></i> Analisis
                  </a>
                </li>
              </ul>

              <!-- Tab panes -->
              <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="tabSoal">
                  <div class="button-floating-right-top">
                    <a href="#" class="btn btn-pn-primary btn-sm">
                      <i class="fa fa-plus-circle"></i> Tambah Soal
                    </a>
                    <a href="#" class="btn btn-primary btn-sm">
                      <i class="fa fa-file-text-o"></i> Bank Soal
                    </a>
                  </div>
                  <div class="table-empty">
                    <p>
                     Data Soal Anda Masih Kosong
                    </p>
                    <a href="#" class="btn btn-pn-primary btn-sm">
                     <i class="fa fa-plus-circle"></i> Tambah Soal
                    </a>
                  </div><!-- /.table-empty -->
                </div>
                <div role="tabpanel" class="tab-pane" id="tabAnalisis">
                  <div class="table-empty">
                    <p>
                     Data Analisis Anda Masih Kosong
                    </p>
                    <a href="#" class="btn btn-pn-primary btn-sm">
                     <i class="fa fa-plus-circle"></i> Tambah Soal
                    </a>
                  </div><!-- /.table-empty -->
                </div>
              </div>
            </div>
          </div>
            
          </div><!-- /.col-card -->
        </div><!-- /.col-md-12 -->
      </div><!-- /.row -->
    </div><!-- /.col-md-12 -->
  </div><!-- /.row -->
</div><!-- /.container-fluid -->
<script type="text/javascript" src="libraries/canvasjs-1.8.0/canvasjs.min.js"></script>
<script type="text/javascript" src="scripts/column_chart.js"></script>
