<div class="container-fluid">
  <div class="row">
    <div class="col-lg-12">
      <br><br>
      <div class="text-center">
        <h1>Selamat Datang di Pinisi Edubox!</h1>
        <p>Bapak <strong>Galih Pratama</strong> belum mempunyai kegiatan apapun, untuk memulai silahkan:</p>
        <div class="getstarted-action">
          <div class="col-md-6 col-md-push-3">
            <a href="#" class="btn btn-pn-primary btn-lg btn-block">Buat Ujian</a>
          </div>
          <div class="clearfix"></div>
          <p>
            <small>atau masuk ke halaman <a href="#">pelajaran</a>.</small>
          </p>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript" src="libraries/canvasjs-1.8.0/canvasjs.min.js"></script>
<script type="text/javascript" src="scripts/pie_chart.js"></script>
