<!DOCTYPE html>
<html>
    <head>
        <?php include '_head.php'; ?>
    </head>
    <body>
        <?php
            if ($_GET['page'] == 'quiz' || $_GET['page'] == 'quiz-block' || $_GET['page'] == 'quiz-essay-draw') {
              ?>
                <style>
                    #sidebar-wrapper {
                        display: none !important; 
                        height: 0 !important;
                        width:0 !important; 
                        padding-left: 0 !important; 
                        background: #F7F7F7;
                    }
                    #wrapper.toggled {
                        padding-left: 0 !important;
                        background: #F7F7F7;
                    }
                    #wrapper.toggled #sidebar-wrapper {
                        display: none !important; 
                        width:0 !important; 
                        padding-left: 0 !important; 
                        height: 0 !important;
                        background: #F7F7F7;
                    }
                </style>
              <?php
            }
        ?>
        <div id="wrapper">

            <!-- Navbar -->
            <?php include '_dashboard_navbar.php'; ?>

            <!-- Sidebar -->
            <?php include '_dashboard_sidebar.php'; ?>

            <!-- Page Content -->
            <div id="page-content-wrapper">
                <?php
                switch ($_GET['page']) {
                    case 'home':
                        include '_dashboard_page_home.php';
                        break;

                    case 'home-student':
                        include '_dashboard_page_home_student.php';
                        break;

                    case 'home-developer':
                        include '_dashboard_page_home_developer.php';
                        break;

                    case 'home-pengawas':
                        include '_dashboard_page_home_pengawas.php';
                        break;

                    case 'getstarted':
                        include '_dashboard_page_getstarted.php';
                        break;

                    case 'exam':
                        include '_dashboard_page_exam.php';
                        break;

                    case 'exam-sync':
                        include '_dashboard_page_exam_sync.php';
                        break;

                    case 'exam-review':
                        include '_dashboard_page_exam_review.php';
                        break;

                    case 'exam-admin':
                        include '_dashboard_page_exam_admin.php';
                        break;

                    case 'exam-detail':
                        include '_dashboard_page_exam_detail.php';
                        break;

                    case 'exam-detail-merge':
                        include '_dashboard_page_exam_detail_merge.php';
                        break;

                    case 'exam-detail-essay':
                        include '_dashboard_page_exam_detail_essay.php';
                        break;

                    case 'exam-add':
                        include '_dashboard_page_add_exam.php';
                        break;

                    case 'task':
                        include '_dashboard_page_task.php';
                        break;

                    case 'course':
                        include '_dashboard_page_course.php';
                        break;

                    case 'course-detail':
                        include '_dashboard_page_course_detail.php';
                        break;

                    case 'course-detail-bank':
                        include '_dashboard_page_course_detail_bank.php';
                        break;

                    case 'course-student':
                        include '_dashboard_page_course_student.php';
                        break;

                    case 'course-student-detail':
                        include '_dashboard_page_course_student_detail.php';
                        break;

                    case 'course-student-detail-video':
                        include '_dashboard_page_course_student_detail_video.php';
                        break;

                    case 'course-student-detail-attachment':
                        include '_dashboard_page_course_student_detail_attachment.php';
                        break;

                    case 'course-student-detail-text':
                        include '_dashboard_page_course_student_detail_text.php';
                        break;

                    case 'exam-detail-empty':
                        include '_dashboard_page_exam_detail_empty.php';
                        break;

                    case 'add-soal':
                        include '_dashboard_page_add_soal.php';
                        break;

                    case 'add-soal-essay':
                        include '_dashboard_page_add_soal_essay.php';
                        break;

                    case 'add-soal-preview':
                        include '_dashboard_page_add_soal_preview.php';
                        break;

                    case 'grade-add-daily':
                        include '_dashboard_page_add_daily_grade.php';
                        break;

                    case 'score':
                        include '_dashboard_page_score.php';
                        break;

                    case 'quiz':
                        include '_dashboard_page_quiz.php';
                        break;

                    case 'quiz-block':
                        include '_dashboard_page_quiz_block.php';
                        break;

                    case 'quiz-essay':
                        include '_dashboard_page_quiz_essay.php';
                        break;

                    case 'quiz-essay-draw':
                        include '_dashboard_page_quiz_essay_draw.php';
                        break;

                    default:
                        include '_dashboard_not_found.php';
                        break;
                }
                ?>
            </div>
            <!-- /#page-content-wrapper -->
        </div>
        <!-- /#wrapper -->
    </body>
    <?php include '_foot.php'; ?>
    <script type="text/javascript" src="scripts/dashboard.js"></script>
    <script type="text/javascript" src="scripts/step_wizard.js"></script>

</html>
