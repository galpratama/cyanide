<div class="container-fluid">
  <div class="row">
    <div class="col-lg-12">
      <div class="alert alert-success  alert-dismissable">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <p>
          <strong>Danger!</strong> Indicates a dangerous or potentially negative action.
        </p>
        <p>
          <div class="row">
            <div class="col-md-6">
              <a href="#" class="btn btn-success btn-block btn-margin-mobile">Action 1</a>
            </div><!-- /.col-md-6 -->
            <div class="col-md-6">
              <a href="#" class="btn btn-danger btn-block btn-margin-mobile">Action 1</a>
            </div><!-- /.col-md-6 -->
          </div><!-- /.row -->
        </p>
      </div>
    </div><!-- /.col-lg-12 -->
    <div class="col-lg-12">
        <h3>Ujian Terkini
          <small class="hidden-xs">Daftar Ujian Terbaru Anda</small>
          <a href="#" class="btn btn-sm btn-pn-gray btn-pn-round pull-right">
            <span class="hidden-sm hidden-xs">SELENGKAPNYA</span>
            <span class="hidden-md hidden-lg"><i class="fa fa-chevron-right"></i></span>
          </a>
        </h3>
        <div class="row">
          <?php include '_card_exam.php'; ?>
          <?php include '_card_blank_add_exam.php'; ?>
        </div>
        <h3>Tugas Terkini
          <small class="hidden-xs">Daftar Tugas Terbaru Anda</small>
          <a href="#" class="btn btn-sm btn-pn-gray btn-pn-round pull-right">
            <span class="hidden-sm hidden-xs">SELENGKAPNYA</span>
            <span class="hidden-md hidden-lg"><i class="fa fa-chevron-right"></i></span>
          </a>
        </h3>
        <div class="row">
          <?php include '_card_task.php'; ?>
          <?php include '_card_blank_add_task.php'; ?>
        </div>
      </div>
  </div>
</div>

<script type="text/javascript" src="libraries/canvasjs-1.8.0/canvasjs.min.js"></script>
<script type="text/javascript" src="scripts/pie_chart.js"></script>
