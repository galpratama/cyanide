<!-- Modal -->
<div class="modal fade" id="modalPanduanImportWord" tabindex="-1" role="dialog" aria-labelledby="filterPanduanImportWord">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-center" id="forgotPasswordLabel">Panduan Tambah Soal Wizard</h4>
      </div>
      <div class="modal-body">
        <h4><i class="fa fa-info-circle"></i> Panduan Menggunakan <strong>Tambah Soal Wizard</strong>:</h4>
        <p>
          Bentuk format teks yang dapat diterima:
          <ul>
            <li>
              Penomoran soal tidak menggunakan numbering
            </li>
            <li>
              Abjad pilihan jawaban tidak menggunakan bullet/numbering
            </li>
            <li>
              Abjad pilihan menggunakan huruf kapital
            </li>
            <li>
              Soal dan pilihan jawaban dipisah menggunakan kalimat <strong>"KUNCI : [Abjad kunci jawaban]"</strong>
            </li>
          </ul>
        </p>
        <h4><i class="fa fa-check-circle"></i> Contoh format soal yang benar:</h4>
        <p>
          <img src="images/contoh-import-word.png" alt="" class="thumbnail" style="max-width: 100%;">
        </p>
      </div>
    </div>
  </div>
</div>
