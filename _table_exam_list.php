<tr>
  <th class="text-center">1</th>
  <td class="collapsible-row-container">
    <div class="collapsible-row">
      <div>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit rerum voluptas tempore officiis! Iste illo impedit, deserunt, a vero at laborum deleniti explicabo, harum veritatis quas temporibus molestiae sit dicta.?
        </p>
      </div>
      <h4>Informasi Soal</h4>
      <div class="table-responsive">
        <table class="table table-bordered table-row-details">
          <tr>
            <th>Kualitas Soal</th>
            <td><span class="label label-danger">Sukar</span></td>
          </tr>
          <tr>
            <th>Validitas</th>
            <td>0.82</td>
          </tr>
          <tr>
            <th>Reliabilitas</th>
            <td>0.7</td>
          </tr>
          <tr>
            <th>Daya Pembeda</th>
            <td>0.6</td>
          </tr>
        </table>
      </div>
      <div class="exam-answer">
        <h4>Pilihan Jawaban</h4>
        <div class="alert alert-success" role="alert">
          Jawaban Paling Benar Sealam Semesta
        </div>
        <div class="alert alert-danger" role="alert">
          Jawaban Jelek
        </div>
        <div class="alert alert-danger" role="alert">
          Jawaban Jelek
        </div>
        <div class="alert alert-danger" role="alert">
          Jawaban Jelek
        </div>
        <div class="alert alert-danger" role="alert">
          Jawaban Jelek
        </div>
      </div>
    </div>
  </td>
  <td>
    <span class="text-red text-bold">80%</span>
  </td>
  <td>
    <span class="text-green text-bold">20%</span>
  </td>
  <td>
  <a href="dashboard.php?page=edit-exam" class="btn btn-primary btn-xs">
    <i class="fa fa-pencil"></i>
  </a>
  </td>
</tr>
