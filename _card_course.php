<div class="col-md-4">
    <div class="col-card row">
      <div class="col-md-12">
        <div class="row">
          <div class="title-top-left">
              <h3>Matematika XII-IPA-1</h3>
              <p>
                  <strong>Senin</strong> 09:00 - 10:30
                  <br>
                  <strong>Rabu</strong> 06:45 - 08:15
              </p>
          </div>
        </div>
      </div>
      <div class="col-md-12">
        <br>
        <div class="chart-pie">
          <div id="chartPieContainer1"
               style="height: 200px; width: 100%;">
          </div>
          <div class="total" id="total-1">300</div>
        </div>
        <br>
      </div>
      <div class="col-md-12">
          <div class="row no-gutter row-accumulate">
              <div class="col-md-6 col-sm-6 col-xs-6 row-accumulate-left">
                  <div class="accumulate-score accumulate-red">40</div>
                  <div class="accumulate-description">
                      Siswa
                  </div>
              </div>
              <div class="col-md-6 col-sm-6 col-xs-6 row-accumulate-right">
                  <div class="accumulate-score accumulate-green">5</div>
                  <div class="accumulate-description">
                      Aktifitas
                  </div>
              </div>
          </div>
      </div>
      <div class="col-md-12">
         <div class="row row-btn-integrated-card no-gutter">
             <br>
             <a href="dashboard.php?page=course-detail" type="button" class="btn btn-pn-primary btn-pn-card-integrated btn-lg btn-block" name="button">
                 SELENGKAPNYA
             </a>
         </div>
      </div>
      <div class="clearfix"></div>
    </div>
</div>
