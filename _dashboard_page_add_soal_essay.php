<div class="container-fluid">
  <div class="row">
    <?php include '_breadcrumb_add_soal.php'; ?>
    <div class="col-lg-12">
      <h2>Buat Soal Baru</h2>
      <div class="row">
        <div class="col-md-12">
          <h4>Informasi Soal</h4>
          <div class="col-card">
            <div class="col-dropdown">
              <div class="row">
                <div class="col-md-9">
                  <label for="kompetensiDasar">Kompetensi Dasar</label>
                  <select
                    name="kompetensiDasar"
                    class="selectpicker form-control"
                    data-style="btn-default input-lg"
                    data-live-search="true"
                    title="Kompetensi Dasar">
                    <option>Isi Kompetensi Dasar</option>
                    <option>Isi Kompetensi Dasar</option>
                    <option>Isi Kompetensi Dasar</option>
                    <option>Isi Kompetensi Dasar</option>
                  </select>
                </div>
                <br class="visible-xs">
                <div class="col-md-3">
                  <label for="jenisSoal">Jenis Soal</label>
                  <select name="jenisSoal" class="selectpicker form-control" data-style="btn-default input-lg">
                    <option>Pilihan Ganda</option>
                    <option selected>Essay</option>
                  </select>
                </div>
              </div>
            </div>
          </div><!-- /.col-card -->
        </div><!-- /.col-md-8 -->
        <div class="col-md-12">
          <h4>Tambah Soal Essay</h4>
          <div class="col-card">
            <div class="row">
              <div class="col-xs-12">
                  <ul class="nav nav-pills nav-justified setup-panel">
                      <li class="active"><a href="#step-1">
                          <h4 class="list-group-item-heading">Tahap 1</h4>
                          <p class="list-group-item-text">Buat Soal Essay </p>
                      </a></li>
                      <li class="disabled"><a href="#step-2">
                          <h4 class="list-group-item-heading">Tahap 2</h4>
                          <p class="list-group-item-text"></p>Tambahkan Soal!</p>
                      </a></li>
                  </ul>
              </div>
            </div>
          </div>
          <div class="col-card">
            <div class="row">
              <div class="col-md-4">
                <h3>Bobot Poin</h3>
                <select name="jenisSoal" class="form-control selectpicker" data-style="btn-default input-lg">
                  <option>10</option>
                  <option>20</option>
                  <option>30</option>
                  <option>40</option>
                  <option>50</option>
                </select>
                <br><br>
              </div>
              <div class="col-md-8">
                <h3>Teks Bantuan</h3>
                <input type="text" class="form-control input-lg" placeholder="Mis: Anda dapat mengisi jawaban sesuai dengan nama makanan">
              </div>
              <div class="clearfix"></div>
              <div class="col-md-12">
                <br>
                <h3>Soal Essay</h3>
                <div class="rich-textarea-container">
                  <textarea name="" id="" cols="30" rows="10" class="rich-textarea">Makanan apa yang digoreng?</textarea>
                </div>
              </div>
              <div class="col-md-12">
                <h3>Jawaban Benar</h3>
                <div class="section-jawaban-essay">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Jawaban Benar" value="Nasi Goreng">
                    <span class="input-group-btn">
                      <button class="btn btn-danger" type="button"><i class="fa fa-times"></i></button>
                    </span>
                  </div><!-- /input-group -->
                  <br>
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Jawaban Benar" value="Ayam Goreng">
                    <span class="input-group-btn">
                      <button class="btn btn-danger" type="button"><i class="fa fa-times"></i></button>
                    </span>
                  </div><!-- /input-group -->
                  <br>
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Jawaban Benar" value="Mie Goreng">
                    <span class="input-group-btn">
                      <button class="btn btn-danger" type="button"><i class="fa fa-times"></i></button>
                    </span>
                  </div><!-- /input-group -->
                  <br>
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Jawaban Benar" value="Bebek Goreng">
                    <span class="input-group-btn">
                      <button class="btn btn-danger" type="button"><i class="fa fa-times"></i></button>
                    </span>
                  </div><!-- /input-group -->
                  <br>
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Jawaban Benar">
                    <span class="input-group-btn">
                      <button class="btn btn-danger" type="button"><i class="fa fa-times"></i></button>
                    </span>
                  </div><!-- /input-group -->                  <br>
                  <a href="#"><i class="fa fa-plus-circle"></i> Klik disini untuk menambahkan jawaban baru</a>
                </div>
              </div>
            </div>
          </div>
         </div>
        <div class="col-md-12">
          <div>
            <div class="row">
              <div class="col-md-6 col-md-offset-3">
                <button type="button" class="btn btn-warning btn-sm btn-block next-step"><i class="fa fa-plus-circle"></i> Tambah Soal Essay</button>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="col-card">
            <div class="row">
              <div class="col-md-6">
                <button type="button" class="btn btn-primary btn-lg btn-pn-round btn-block next-step"><i class="fa fa-save"></i> Simpan</button>
              </div>
              <div class="col-md-6">
                <button type="button" class="btn btn-pn-primary btn-lg btn-pn-round btn-block next-step">Buat Soal <i class="fa fa-arrow-right"></i></button>
              </div>
            </div>
          </div>
        </div>
        </div><!-- /.col-md-8 -->
        </div><!-- /.row -->
      </div><!-- /.row -->
    </div><!-- /.col-md-12 -->
  </div><!-- /.row -->
</div><!-- /.container-fluid -->
<script type="text/javascript" src="libraries/canvasjs-1.8.0/canvasjs.min.js"></script>
<script type="text/javascript" src="scripts/column_chart.js"></script>
