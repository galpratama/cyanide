<div id="sidebar-wrapper" style="">
  <ul class="sidebar-nav">
    <div class="sidebar-profile">
      <div class="sidebar-profile-picture">
        <img src="https://galihpratama.net/wp-content/uploads/2016/05/2016-Picture.png"
            alt="Profile Picture" class="img-circle" style="width:50px;height:50px">
      </div>
      <div class="sidebar-profile-text">
        <h4>Galih Pratama</h4>
        <p>Guru Umum</p>
      </div>
    </div>
    <li class="<?php echo '', ($_GET['page'] == 'home' ? 'active' : '') ?>">
        <a href="dashboard.php?page=home"><i class="icon-sidebar icon-pn-home"></i> Beranda</a>
    </li>
      <li class="<?php echo '', ($_GET['page'] == 'home' ? 'active' : '') ?>">
          <a href="dashboard.php?page=course-student"><i class="icon-sidebar icon-pn-course"></i> Pelajaran (Siswa)</a>
      </li>
    <li class="<?php echo '', ($_GET['page'] == 'course' ? 'active' : '') ?>">
        <a href="dashboard.php?page=course">
          <i class="icon-sidebar icon-pn-course"></i> Pelajaran (Guru)
        </a>
        <ul class="sidebar-child">
          <li>
            <a href="dashboard.php?page=course">
              XII-RPL-B Matematika
            </a>
          </li>
          <li>
            <a href="dashboard.php?page=course">
              XII-RPL-B Bahasa Indonesia Panjang Banget
            </a>
          </li>
          <li>
            <a href="dashboard.php?page=course">
              XII-RPL-B Kimia
            </a>
          </li>
          <li>
            <a href="dashboard.php?page=course">
              XII-RPL-B Fisika
            </a>
          </li>
        </ul>
    </li>

    <li class="<?php echo '', ($_GET['page'] == 'exam' || $_GET['page'] == 'exam-detail' ? 'active' : '') ?>">
        <a href="dashboard.php?page=exam">
          <i class="icon-sidebar icon-pn-exam"></i> Ujian
        </a>
        <button
          onclick="location.href='dashboard.php?page=exam-add';"
          class="btn btn-sidebar-action btn-xs btn-success">
          <i class="fa fa-plus"></i>
        </button>
        <button
          onclick="location.href='dashboard.php?page=exam-sync';"
          class="btn btn-sidebar-action btn-xs btn-success" style="margin-right: 40px;">
          <i class="fa fa-refresh"></i>
        </button>
    </li>
    <li class="<?php echo '', ($_GET['page'] == 'task' ? 'active' : '') ?>">
        <a href="dashboard.php?page=task">
          <i class="icon-sidebar icon-pn-task"></i> Tugas
        </a>
        <button
          onclick="location.href='dashboard.php?page=task-add';"
          class="btn btn-sidebar-action btn-xs btn-success">
          <i class="fa fa-plus"></i>
        </button>
    </li>
    <li class="<?php echo '', ($_GET['page'] == 'grade' ? 'active' : '') ?>">
        <a href="#" data-toggle="modal" data-target="#modalFilterScore">
          <i class="icon-sidebar icon-pn-grade"></i> Nilai
        </a>
        <button
        onclick="location.href='dashboard.php?page=grade-add-daily';"
        class="btn btn-sidebar-action btn-xs btn-success">
          <i class="fa fa-plus"></i> Penilaian Harian
        </button>
    </li>
  </ul>
</div>
