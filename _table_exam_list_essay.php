<tr>
  <th class="text-center">1</th>
  <td class="collapsible-row-container">
    <div class="collapsible-row">
      <div>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit rerum voluptas tempore officiis! Iste illo impedit, deserunt, a vero at laborum deleniti explicabo, harum veritatis quas temporibus molestiae sit dicta.?
        </p>
      </div>
      <h4>Informasi Soal</h4>
      <div class="table-responsive">
        <table class="table table-bordered table-row-details">
          <tr>
            <th>Kualitas Soal</th>
            <td><span class="label label-danger">Sukar</span></td>
          </tr>
          <tr>
            <th>Validitas</th>
            <td>0.82</td>
          </tr>
          <tr>
            <th>Reliabilitas</th>
            <td>0.7</td>
          </tr>
          <tr>
            <th>Daya Pembeda</th>
            <td>0.6</td>
          </tr>
        </table>
      </div>
      <div class="exam-answer">
        <h4>Jawaban Benar</h4>
        <div class="alert alert-success" role="alert">
          Nasi Goreng
        </div>
        <div class="alert alert-success" role="alert">
          Kambing Goreng
        </div>
        <div class="alert alert-success" role="alert">
          Mie Goreng
        </div>
        <div class="alert alert-success" role="alert">
          Sapi Goreng
        </div>
      </div>
    </div>
  </td>
  <td>
    <span class="text-red text-bold">80%</span>
  </td>
  <td>
    <span class="text-green text-bold">20%</span>
  </td>
  <td>
  <a href="dashboard.php?page=edit-exam" class="btn btn-primary btn-xs">
    <i class="fa fa-pencil"></i>
  </a>
    <a href="dashboard.php?page=exam-review" class="btn btn-danger btn-xs">
      <i class="fa fa-eye"></i> Review Jawaban Siswa
    </a>
  </td>
</tr>
