<div class="not-found-page">
  <div class="text-center">
    <img src="images/sad.png" alt="" width="100">
    <h1>404 Not Found</h1>
    <p>Maaf, halaman yang anda cari tidak ditemukan.</p>
  </div>
</div>
