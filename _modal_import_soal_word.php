<!-- Modal -->
<div class="modal fade" id="modalImportSoalWord" tabindex="-1" role="dialog" aria-labelledby="filterImportSoalWord">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-center" id="forgotPasswordLabel">Sunting Soal</h4>
      </div>
      <div class="modal-body">
        <div>
          <textarea class="simple-textarea" name="name" rows="8" cols="40">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid consectetur fuga accusantium, non dignissimos architecto.
          </textarea>
        </div>
        <div>
          <br>
          <button type="submit" class="btn btn-pn btn-pn-primary btn-lg btn-block">Simpan</button>
        </div>
      </div>
    </div>
  </div>
</div>
