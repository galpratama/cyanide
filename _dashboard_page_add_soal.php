<div class="container-fluid">
  <div class="row">
    <?php include '_breadcrumb_add_soal.php'; ?>
    <div class="col-lg-12">
      <h2>Buat Soal Baru</h2>
      <div class="row">
        <div class="col-md-8">
          <h4>Tambah Soal Wizard</small></h4>
          <div class="col-card">
            <div class="row">
              <div class="col-xs-12">
                <ul class="nav nav-pills nav-justified setup-panel">
                  <li class="active"><a href="#step-1">
                      <h4 class="list-group-item-heading">Tahap 1</h4>
                      <p class="list-group-item-text">Import dari Word </p>
                    </a></li>
                  <li class="disabled"><a href="#step-2">
                      <h4 class="list-group-item-heading">Tahap 2</h4>
                      <p class="list-group-item-text">Preview dan Sunting Soal</p>
                    </a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-card">
            <div class="row">
              <div class="col-md-12">
                <div class="text-center hide">
                  <img src="images/import-icon.png" alt="" class="pull-left" style="width: 110px;">
                  <h3>Tambahkan Soal dari dokumen <strong>Microsoft Word</strong> dibawah ini!</h3>
                  <p>
                    Anda dapat menyalin dan menempelkan (<i>copy & paste</i>) soal yang ingin anda masukkan di kotak editor dibawah ini.
                    <br>
                    Atau anda dapat mengetikan langsung soal dan jawaban dibawah ini.
                  </p>
                  <p>
                    <a href="#" data-toggle="modal" data-target="#modalPanduanImportWord" class="btn btn-pn-primary btn-sm">
                      <i class="fa fa-question-circle"></i> Panduan Menggunakan <strong>Import dari Word</strong>
                    </a>
                  </p>
                </div>
                <div class="clearfix"></div>
                <div class="rich-textarea-container">
                  <textarea name="" id="" cols="30" rows="10" class="rich-textarea"></textarea>
                </div>
              </div>
            </div>
          </div>
         </div>
        <div class="col-md-4">
          <h4>Informasi Soal</h4>
          <div class="col-card">
            <div class="col-dropdown">
              <div class="row">
                <div class="col-md-12">
                  <label for="kompetensiDasar">Kompetensi Dasar</label>
                  <select
                      name="kompetensiDasar"
                      class="selectpicker form-control"
                      data-style="btn-default input-lg"
                      data-live-search="true"
                      title="Kompetensi Dasar">
                    <option>Isi Kompetensi Dasar</option>
                    <option>Isi Kompetensi Dasar</option>
                    <option>Isi Kompetensi Dasar</option>
                    <option>Isi Kompetensi Dasar</option>
                  </select>
                </div>
              </div>
              <br><br>
              <div class="row">
                <div class="col-md-12">
                  <label for="jenisSoal">Jenis Soal</label>
                  <select name="jenisSoal" class="selectpicker form-control" data-style="btn-default input-lg">
                    <option>Pilihan Ganda</option>
                    <option>Essay</option>
                  </select>
                </div>
              </div>
            </div>
          </div><!-- /.col-card -->
          <h4>Aksi Soal</h4>
          <div class="col-card">
            <div class="row">
              <div class="col-md-12">
                <button type="button" class="btn btn-warning btn-lg btn-block next-step" style="margin-bottom: 5px"><i class="fa fa-eye"> Pratinjau</i></button>
              </div>
              <div class="col-md-12">
                <button type="button" class="btn btn-pn-primary btn-lg btn-block next-step"><i class="fa fa-save"></i> Simpan</button>
              </div>
            </div>
          </div>
        </div><!-- /.col-md-8 -->
        </div><!-- /.col-md-8 -->
        </div><!-- /.row -->
      </div><!-- /.row -->
    </div><!-- /.col-md-12 -->
  </div><!-- /.row -->
</div><!-- /.container-fluid -->
<script type="text/javascript" src="libraries/canvasjs-1.8.0/canvasjs.min.js"></script>
<script type="text/javascript" src="scripts/column_chart.js"></script>
