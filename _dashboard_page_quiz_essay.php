<div class="container-fluid">
  <div class="row">
    <div class="col-lg-12 container-soal-floating">
        <h2 class="pull-left">
          Kuis Essay Ujian Coba
          <br>
          <small>Waktu Pengerjaan : 120 Menit</small>
        </h2>
        <div class="pull-right countdown-right">
          <br>
          <div class="clock-timer" id="clock"></div>
          <a href="#" class="btn btn-pn-primary hide"><i class="fa fa-check"></i> Cek Jawaban</a>
        </div>
        <div class="clearfix"></div>
        <div class="row">
          <div class="col-md-3">
            <div class="row">
              <div class="switch-quiz-box">
                <div class="col-md-12">
                  <h4 class="hidden-xs hidden-sm">Daftar Soal</h4>
                  <div class="col-card">
                    <div class="list-soal-container">
                      <ul class="nav nav-pills">
                        <div class="hidden-md hidden-lg">
                          &nbsp;&nbsp;<i class="fa fa-arrow-up fa-2x"></i>
                        </div>
                        <li class="list-soal active"><a href="#step1" id="coba1" data-local="768" class="btn btn-soal" data-toggle="tab" data-step="1">1</a></li>
                        <li class="list-soal"><a href="#step2" id="coba2" data-local="769" class="btn btn-soal" data-toggle="tab" data-step="2" >2</a></li>
                        <li class="list-soal"><a href="#step2" id="coba2" data-local="769" class="btn btn-soal" data-toggle="tab" data-step="2" >3</a></li>
                        <li class="list-soal"><a href="#step2" id="coba2" data-local="769" class="btn btn-soal" data-toggle="tab" data-step="2" >4</a></li>
                        <li class="list-soal"><a href="#step2" id="coba2" data-local="769" class="btn btn-soal" data-toggle="tab" data-step="2" >5</a></li>
                        <li class="list-soal"><a href="#step2" id="coba2" data-local="769" class="btn btn-soal" data-toggle="tab" data-step="2" >6</a></li>
                        <li class="list-soal"><a href="#step2" id="coba2" data-local="769" class="btn btn-soal" data-toggle="tab" data-step="2" >7</a></li>
                        <li class="list-soal"><a href="#step2" id="coba2" data-local="769" class="btn btn-soal" data-toggle="tab" data-step="2" >8</a></li>
                        <li class="list-soal"><a href="#step2" id="coba2" data-local="769" class="btn btn-soal" data-toggle="tab" data-step="2" >9</a></li>
                        <li class="list-soal"><a href="#step2" id="coba2" data-local="769" class="btn btn-soal" data-toggle="tab" data-step="2" >10</a></li>
                        <li class="list-soal"><a href="#step2" id="coba2" data-local="769" class="btn btn-soal" data-toggle="tab" data-step="2" >7</a></li>
                        <li class="list-soal"><a href="#step2" id="coba2" data-local="769" class="btn btn-soal" data-toggle="tab" data-step="2" >8</a></li>
                        <li class="list-soal"><a href="#step2" id="coba2" data-local="769" class="btn btn-soal" data-toggle="tab" data-step="2" >9</a></li>
                        <li class="list-soal"><a href="#step2" id="coba2" data-local="769" class="btn btn-soal" data-toggle="tab" data-step="2" >10</a></li>
                        <div class="hidden-md hidden-lg">
                          &nbsp;&nbsp;<i class="fa fa-arrow-down fa-2x"></i>
                        </div>
                      </ul>
                    </div>
                  </div>
                  <div class="progress-container">
                    <div class="progress">
                      <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="8" style="width: 80%;">Soal 6 dari 18 Soal</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-9">
            <h4>Soal</h4>
            <div class="col-card">
              <div>
                <h3>
                  Soal 1
                </h3>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero asperiores, accusantium, tempore minus quidem incidunt illum vero corporis mollitia quis exercitationem ut, consectetur! Placeat nesciunt magni, modi consequatur tempora. Laborum mollitia at asperiores necessitatibus vel beatae, provident alias sit! Neque magnam ipsam architecto corrupti inventore dolores porro atque similique deleniti, assumenda doloremque non ipsa at, dolorem harum expedita voluptatem nobis reiciendis iure quibusdam laborum culpa. Sit cumque ea placeat nihil nobis vero eligendi eveniet perferendis nesciunt porro unde quibusdam animi quae, quo ipsa, numquam, commodi tempore laborum amet molestias labore totam! Dolorum at suscipit, pariatur, necessitatibus vel accusamus ex cupiditate!
                </p>
                <div class="well">
                  <i class="fa fa-question-circle"></i> <strong>Bantuan:</strong> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, quae!
                </div>
                <div class="rich-textarea-container">
                  <textarea name="" id="" cols="30" rows="10" class="rich-textarea">Nasi Goreng</textarea>
                </div>
              </div>
            </div>
            <div class="row col-button">
              <div class="col-md-6">
                <a href="#" class="btn btn-warning btn-lg btn-block">
                  <i class="fa fa-chevron-left"></i> Sebelumnya
                </a>
                <br>
              </div>
              <div class="col-md-6">
                <a href="#" class="btn btn-pn-primary btn-lg btn-block">
                  Selanjutnya <i class="fa fa-chevron-right"></i>
                </a>
              </div>
            </div><!-- /.col-button -->
          </div>
        </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="libraries/canvasjs-1.8.0/canvasjs.min.js"></script>
<script type="text/javascript" src="libraries/jquery/jquery.min.js"></script>
<script type="text/javascript" src="libraries/countdown/jquery.countdown.js"></script>
<script type="text/javascript" src="scripts/quiz.js"></script>
