<!-- Modal -->
<div class="modal fade" id="modalForgotPassword" tabindex="-1" role="dialog" aria-labelledby="forgotPasswordLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="forgotPasswordLabel">Lupa Password?</h4>
      </div>
      <div class="modal-body">
        Silahkan masukkan NIS, NIP, atau Email anda untuk menyetel ulang sandi anda.
        <br><br>
        <form action="#">
            <div class="form-group">
                <input type="text" class="form-control input-lg input-pn input-pn-center" placeholder="NIS, NIP, atau Email anda">
            </div>
            <button type="submit" class="btn btn-pn btn-pn-primary btn-lg">Atur Ulang Kata Sandi</button>
        </form>

      </div>
    </div>
  </div>
</div>
