<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div id="bc1" class="btn-group btn-breadcrumb">
        <a href="dashboard.php?page=home" class="btn btn-default"><i class="fa fa-home"></i> Beranda</a>
        <a href="dashboard.php?page=exam" class="btn btn-default"><div>Ujian</div></a>
        <a href="#" class="btn btn-default"><div>Matematika</div></a>
        <a href="#" class="btn btn-success"><div>Ujian 1 Matematika</div></a>
      </div>
    </div><!-- /.col-md-12 -->
    <div class="col-lg-12">
      <h2>Ujian 1 Matematika</h2>
      <div class="row">
        <div class="col-md-8">
          <h4>Grafik Sebaran Nilai</h4>
          <div class="col-card">
            <div class="chart-pie">
              <div id="chartContainer"
                   style="height: 323px; width: 100%;">
              </div>
              <br />
              <div class="text-center">Sebaran Nilai</div><!-- /.text-center -->
            </div>
          </div><!-- /.col-card -->
        </div><!-- /.col-md-8 -->
        <div class="col-md-4">
          <h4>Informasi dan Status</h4>
          <div class="col-card card-exam-switch">
            <div class="pull-left">
              <h4>
                Status Ujian <span class="label label-danger">Nonaktif</span>
              </h4>
            </div><!-- /.pull-left -->
            <div class="pull-right">
              <div class="pn-switch">
                  <input type="checkbox" name="switch1" class="pn-switch-checkbox" id="switch1">
                  <label class="pn-switch-label" for="switch1"></label>
              </div>
            </div><!-- /.pull-right -->
            <div class="clearfix"></div><!-- /.clearfix -->
          </div><!-- /.col-card -->
          <div class="col-card">
            <table class="table table-hover table-condensed">
              <thead>
                  <tr>
                    <th>
                      Informasi
                    </th>
                    <th>
                      Detail
                    </th>
                  </tr>
                </thead>
              <tbody>
                <tr>
                  <td>
                    Waktu Pengerjaan
                  </td>
                  <td>
                    120 Menit
                  </td>
                </tr>
                <tr>
                  <td>
                    Total Pertanyan
                  </td>
                  <td>
                    50
                  </td>
                </tr>
                <tr>
                  <td>
                    Acak Soal
                  </td>
                  <td>
                    Ya
                  </td>
                </tr>
                <tr>
                  <td>
                    Percobaan Kuis
                  </td>
                  <td>
                    1 Kali
                  </td>
                </tr>
                <tr>
                  <td>
                    Status
                  </td>
                  <td>
                    Tampil
                  </td>
                </tr>
                <tr>
                  <td>
                    Kode Pembuka
                  </td>
                  <td>
                    <code>c4dmF</code>
                  </td>
                </tr>
              </tbody>
            </table>
            <!-- Single button -->
            <div class="btn-group btn-block dropup">
              <button type="button" class="btn btn-pn-primary btn-lg btn-block dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-gear"></i> Aksi Ujian <i class="fa fa-caret-up"></i>
              </button>
              <ul class="dropdown-menu btn-block">
                <li><a href="#"><i class="fa fa-pencil"></i> Sunting Informasi Ujian</a></li>
                <li><a href="#"><i class="fa fa-eye"></i> Pratinjau Ujian</a></li>
                <li><a href="#"><i class="fa fa-download"></i> Unduh Nilai Ujian</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="#"><i class="fa fa-copy"></i> Salin Ujian</a></li>
              </ul>
            </div>
          </div><!-- /.col-card -->
        </div><!-- /.col-md-4 -->
      </div><!-- /.row -->
      <div class="row">
        <div class="col-md-12">
          <h4>Detail Ujian</h4>
          <div class="col-card card-floating-button">
            <div>
              <!-- Nav tabs -->
              <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active">
                  <a href="#tabSoal" aria-controls="tabSoal" role="tab" data-toggle="tab">
                    <i class="fa fa-list"></i> Soal
                  </a>
                </li>
                <li role="presentation">
                  <a href="#tabAnalisis" aria-controls="tabAnalisis" role="tab" data-toggle="tab"><i class="fa fa-chart"></i>
                    <i class="fa fa-line-chart"></i> Analisis
                  </a>
                </li>
              </ul>

              <!-- Tab panes -->
              <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="tabSoal">
                  <div class="button-floating-right-top">
                    <a href="dashboard.php?page=add-soal" class="btn btn-pn-primary btn-sm">
                      <i class="fa fa-plus-circle"></i> Tambah Soal
                    </a>
                    <a  href="#" data-toggle="modal" data-target="#modalBankSoal" class="btn btn-primary btn-sm">
                      <i class="fa fa-file-text-o"></i> Bank Soal
                    </a>
                  </div>
                  <div class="exam-list-table">
                    <div class="table-responsive">
                      <table class="table table-hover">
                        <thead>
                          <tr>
                            <th>No.</th>
                            <th>Pertanyaan</th>
                            <th>Jumlah Benar</th>
                            <th>Jumlah Salah</th>
                            <th>Aksi</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <th class="text-center">1</th>
                            <td class="collapsible-row-container">
                              <div class="collapsible-row">
                                <div>
                                  <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit rerum voluptas tempore officiis! Iste illo impedit, deserunt, a vero at laborum deleniti explicabo, harum veritatis quas temporibus molestiae sit dicta.?
                                  </p>
                                </div>
                                <h4>Informasi Soal</h4>
                                <div class="table-responsive">
                                  <table class="table table-bordered table-row-details">
                                    <tr>
                                      <th>Kualitas Soal</th>
                                      <td><span class="label label-danger">Sukar</span></td>
                                    </tr>
                                    <tr>
                                      <th>Validitas</th>
                                      <td>0.82</td>
                                    </tr>
                                    <tr>
                                      <th>Reliabilitas</th>
                                      <td>0.7</td>
                                    </tr>
                                    <tr>
                                      <th>Daya Pembeda</th>
                                      <td>0.6</td>
                                    </tr>
                                  </table>
                                </div>
                                <div class="exam-answer">
                                  <h4>Pilihan Jawaban</h4>
                                  <div class="alert alert-success" role="alert">
                                    Jawaban Paling Benar Sealam Semesta
                                  </div>
                                  <div class="alert alert-danger" role="alert">
                                    Jawaban Jelek
                                  </div>
                                  <div class="alert alert-danger" role="alert">
                                    Jawaban Jelek
                                  </div>
                                  <div class="alert alert-danger" role="alert">
                                    Jawaban Jelek
                                  </div>
                                  <div class="alert alert-danger" role="alert">
                                    Jawaban Jelek
                                  </div>
                                </div>
                              </div>
                            </td>
                            <td>
                              <span class="text-red text-bold">80%</span>
                            </td>
                            <td>
                              <span class="text-green text-bold">20%</span>
                            </td>
                            <td>
                            <a href="dashboard.php?page=edit-exam" class="btn btn-primary btn-xs">
                              <i class="fa fa-pencil"></i>
                            </a>
                            </td>
                          </tr>
                          <tr>
                            <th class="text-center">1</th>
                            <td class="collapsible-row-container">
                              <div class="collapsible-row">
                                <div>
                                  <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit rerum voluptas tempore officiis! Iste illo impedit, deserunt, a vero at laborum deleniti explicabo, harum veritatis quas temporibus molestiae sit dicta.?
                                  </p>
                                </div>
                                <h4>Informasi Soal</h4>
                                <div class="table-responsive">
                                  <table class="table table-bordered table-row-details">
                                    <tr>
                                      <th>Kualitas Soal</th>
                                      <td><span class="label label-danger">Sukar</span></td>
                                    </tr>
                                    <tr>
                                      <th>Validitas</th>
                                      <td>0.82</td>
                                    </tr>
                                    <tr>
                                      <th>Reliabilitas</th>
                                      <td>0.7</td>
                                    </tr>
                                    <tr>
                                      <th>Daya Pembeda</th>
                                      <td>0.6</td>
                                    </tr>
                                  </table>
                                </div>
                                <div class="exam-answer">
                                  <h4>Pilihan Jawaban</h4>
                                  <div class="alert alert-success" role="alert">
                                    Jawaban Paling Benar Sealam Semesta
                                  </div>
                                  <div class="alert alert-danger" role="alert">
                                    Jawaban Jelek
                                  </div>
                                  <div class="alert alert-danger" role="alert">
                                    Jawaban Jelek
                                  </div>
                                  <div class="alert alert-danger" role="alert">
                                    Jawaban Jelek
                                  </div>
                                  <div class="alert alert-danger" role="alert">
                                    Jawaban Jelek
                                  </div>
                                </div>
                              </div>
                            </td>
                            <td>
                              <span class="text-red text-bold">80%</span>
                            </td>
                            <td>
                              <span class="text-green text-bold">20%</span>
                            </td>
                            <td>
                            <a href="dashboard.php?page=edit-exam" class="btn btn-primary btn-xs">
                              <i class="fa fa-pencil"></i>
                            </a>
                            </td>
                          </tr>

                          <tr>
                            <th class="text-center">1</th>
                            <td class="collapsible-row-container">
                              <div class="collapsible-row">
                                <div>
                                  <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit rerum voluptas tempore officiis! Iste illo impedit, deserunt, a vero at laborum deleniti explicabo, harum veritatis quas temporibus molestiae sit dicta.?
                                  </p>
                                </div>
                                <h4>Informasi Soal</h4>
                                <div class="table-responsive">
                                  <table class="table table-bordered table-row-details">
                                    <tr>
                                      <th>Kualitas Soal</th>
                                      <td><span class="label label-danger">Sukar</span></td>
                                    </tr>
                                    <tr>
                                      <th>Validitas</th>
                                      <td>0.82</td>
                                    </tr>
                                    <tr>
                                      <th>Reliabilitas</th>
                                      <td>0.7</td>
                                    </tr>
                                    <tr>
                                      <th>Daya Pembeda</th>
                                      <td>0.6</td>
                                    </tr>
                                  </table>
                                </div>
                                <div class="exam-answer">
                                  <h4>Pilihan Jawaban</h4>
                                  <div class="alert alert-success" role="alert">
                                    Jawaban Paling Benar Sealam Semesta
                                  </div>
                                  <div class="alert alert-danger" role="alert">
                                    Jawaban Jelek
                                  </div>
                                  <div class="alert alert-danger" role="alert">
                                    Jawaban Jelek
                                  </div>
                                  <div class="alert alert-danger" role="alert">
                                    Jawaban Jelek
                                  </div>
                                  <div class="alert alert-danger" role="alert">
                                    Jawaban Jelek
                                  </div>
                                </div>
                              </div>
                            </td>
                            <td>
                              <span class="text-red text-bold">80%</span>
                            </td>
                            <td>
                              <span class="text-green text-bold">20%</span>
                            </td>
                            <td>
                            <a href="dashboard.php?page=edit-exam" class="btn btn-primary btn-xs">
                              <i class="fa fa-pencil"></i>
                            </a>
                            </td>
                          </tr>

                          <tr>
                            <th class="text-center">1</th>
                            <td class="collapsible-row-container">
                              <div class="collapsible-row">
                                <div>
                                  <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit rerum voluptas tempore officiis! Iste illo impedit, deserunt, a vero at laborum deleniti explicabo, harum veritatis quas temporibus molestiae sit dicta.?
                                  </p>
                                </div>
                                <h4>Informasi Soal</h4>
                                <div class="table-responsive">
                                  <table class="table table-bordered table-row-details">
                                    <tr>
                                      <th>Kualitas Soal</th>
                                      <td><span class="label label-danger">Sukar</span></td>
                                    </tr>
                                    <tr>
                                      <th>Validitas</th>
                                      <td>0.82</td>
                                    </tr>
                                    <tr>
                                      <th>Reliabilitas</th>
                                      <td>0.7</td>
                                    </tr>
                                    <tr>
                                      <th>Daya Pembeda</th>
                                      <td>0.6</td>
                                    </tr>
                                  </table>
                                </div>
                                <div class="exam-answer">
                                  <h4>Pilihan Jawaban</h4>
                                  <div class="alert alert-success" role="alert">
                                    Jawaban Paling Benar Sealam Semesta
                                  </div>
                                  <div class="alert alert-danger" role="alert">
                                    Jawaban Jelek
                                  </div>
                                  <div class="alert alert-danger" role="alert">
                                    Jawaban Jelek
                                  </div>
                                  <div class="alert alert-danger" role="alert">
                                    Jawaban Jelek
                                  </div>
                                  <div class="alert alert-danger" role="alert">
                                    Jawaban Jelek
                                  </div>
                                </div>
                              </div>
                            </td>
                            <td>
                              <span class="text-red text-bold">80%</span>
                            </td>
                            <td>
                              <span class="text-green text-bold">20%</span>
                            </td>
                            <td>
                            <a href="dashboard.php?page=edit-exam" class="btn btn-primary btn-xs">
                              <i class="fa fa-pencil"></i>
                            </a>
                            </td>
                          </tr>

                          <tr>
                            <th class="text-center">1</th>
                            <td class="collapsible-row-container">
                              <div class="collapsible-row">
                                <div>
                                  <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit rerum voluptas tempore officiis! Iste illo impedit, deserunt, a vero at laborum deleniti explicabo, harum veritatis quas temporibus molestiae sit dicta.?
                                  </p>
                                </div>
                                <h4>Informasi Soal</h4>
                                <div class="table-responsive">
                                  <table class="table table-bordered table-row-details">
                                    <tr>
                                      <th>Kualitas Soal</th>
                                      <td><span class="label label-danger">Sukar</span></td>
                                    </tr>
                                    <tr>
                                      <th>Validitas</th>
                                      <td>0.82</td>
                                    </tr>
                                    <tr>
                                      <th>Reliabilitas</th>
                                      <td>0.7</td>
                                    </tr>
                                    <tr>
                                      <th>Daya Pembeda</th>
                                      <td>0.6</td>
                                    </tr>
                                  </table>
                                </div>
                                <div class="exam-answer">
                                  <h4>Pilihan Jawaban</h4>
                                  <div class="alert alert-success" role="alert">
                                    Jawaban Paling Benar Sealam Semesta
                                  </div>
                                  <div class="alert alert-danger" role="alert">
                                    Jawaban Jelek
                                  </div>
                                  <div class="alert alert-danger" role="alert">
                                    Jawaban Jelek
                                  </div>
                                  <div class="alert alert-danger" role="alert">
                                    Jawaban Jelek
                                  </div>
                                  <div class="alert alert-danger" role="alert">
                                    Jawaban Jelek
                                  </div>
                                </div>
                              </div>
                            </td>
                            <td>
                              <span class="text-red text-bold">80%</span>
                            </td>
                            <td>
                              <span class="text-green text-bold">20%</span>
                            </td>
                            <td>
                            <a href="dashboard.php?page=edit-exam" class="btn btn-primary btn-xs">
                              <i class="fa fa-pencil"></i>
                            </a>
                            </td>
                          </tr>

                          <tr>
                            <th class="text-center">1</th>
                            <td class="collapsible-row-container">
                              <div class="collapsible-row">
                                <div>
                                  <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit rerum voluptas tempore officiis! Iste illo impedit, deserunt, a vero at laborum deleniti explicabo, harum veritatis quas temporibus molestiae sit dicta.?
                                  </p>
                                </div>
                                <h4>Informasi Soal</h4>
                                <div class="table-responsive">
                                  <table class="table table-bordered table-row-details">
                                    <tr>
                                      <th>Kualitas Soal</th>
                                      <td><span class="label label-danger">Sukar</span></td>
                                    </tr>
                                    <tr>
                                      <th>Validitas</th>
                                      <td>0.82</td>
                                    </tr>
                                    <tr>
                                      <th>Reliabilitas</th>
                                      <td>0.7</td>
                                    </tr>
                                    <tr>
                                      <th>Daya Pembeda</th>
                                      <td>0.6</td>
                                    </tr>
                                  </table>
                                </div>
                                <div class="exam-answer">
                                  <h4>Pilihan Jawaban</h4>
                                  <div class="alert alert-success" role="alert">
                                    Jawaban Paling Benar Sealam Semesta
                                  </div>
                                  <div class="alert alert-danger" role="alert">
                                    Jawaban Jelek
                                  </div>
                                  <div class="alert alert-danger" role="alert">
                                    Jawaban Jelek
                                  </div>
                                  <div class="alert alert-danger" role="alert">
                                    Jawaban Jelek
                                  </div>
                                  <div class="alert alert-danger" role="alert">
                                    Jawaban Jelek
                                  </div>
                                </div>
                              </div>
                            </td>
                            <td>
                              <span class="text-red text-bold">80%</span>
                            </td>
                            <td>
                              <span class="text-green text-bold">20%</span>
                            </td>
                            <td>
                            <a href="dashboard.php?page=edit-exam" class="btn btn-primary btn-xs">
                              <i class="fa fa-pencil"></i>
                            </a>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>

                  </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="tabAnalisis">
                  <div class="button-floating-right-top">
                    <a href="#" class="btn btn-primary btn-sm">
                      <i class="fa fa-download"></i> Unduh ke Berkas Excel
                    </a>
                  </div>
                  <div class="tab-pane-analysis">
                    <div class="row text-center">
                      <div class="col-md-3 col-sm-3">
                        <span class="text-point"><i class="fa fa-arrow-circle-up"></i> Nilai Tertinggi:</span>
                        <span class="text-green text-score">90</span>
                      </div>
                      <div class="col-md-3 col-sm-3">
                        <span class="text-point"><i class="fa fa-arrow-circle-down"></i> Nilai Terendah:</span>
                        <span class="text-green text-score">70</span>
                      </div>
                      <div class="col-md-3 col-sm-3">
                        <span class="text-point"><i class="fa fa-info-circle"></i> Rata-Rata:</span>
                        <span class="text-green text-score">87</span>
                      </div>
                      <div class="col-md-3 col-sm-3">
                        <span class="text-point"><i class="fa fa-info-circle"></i> Standar Deviasi:</span>
                        <span class="text-green text-score">83</span>
                      </div>
                    </div><!-- /.row -->
                    <div class="analysis-table">
                      <div class="table-responsive">
                        <table class="table table-hover table-bordered ">
                          <tr class="table-exam-header">
                            <th rowspan="2">Nama Siswa</th>
                            <th colspan="17" class="text-center">Nomor Soal (urut dari mudah ke sukar)</th>
                            <th rowspan="2">Total</th>
                            <th rowspan="2">Nilai</th>
                          </tr>
                          <tr class="table-exam-header">
                            <td class="table-striped-horizontal">13</td>
                            <td>2</td>
                            <td class="table-striped-horizontal">7</td>
                            <td>6</td>
                            <td class="table-striped-horizontal">9</td>
                            <td>10</td>
                            <td class="table-striped-horizontal">21</td>
                            <td>23</td>
                            <td class="table-striped-horizontal">1</td>
                            <td>4</td>
                            <td class="table-striped-horizontal">5</td>
                            <td>8</td>
                            <td class="table-striped-horizontal">29</td>
                            <td>12</td>
                            <td class="table-striped-horizontal">19</td>
                            <td>17</td>
                            <td class="table-striped-horizontal">18</td>
                          </tr>
                          <tr class="text-center">
                            <td class="text-left">Desi Ratanasari</td>
                            <td class="table-striped-horizontal"><i class="fa fa-check-circle text-primary"></i></td>
                            <td><i class="fa fa-check-circle text-primary"></i></td>
                            <td class="table-striped-horizontal"><i class="fa fa-check-circle text-primary"></i></td>
                            <td><i class="fa fa-check-circle text-primary"></i></td>
                            <td class="table-striped-horizontal"><i class="fa fa-check-circle text-primary"></i></td>
                            <td><i class="fa fa-times-circle text-red"></i></td>
                            <td class="table-striped-horizontal"><i class="fa fa-check-circle text-primary"></i></td>
                            <td><i class="fa fa-check-circle text-primary"></i></td>
                            <td class="table-striped-horizontal"><i class="fa fa-check-circle text-primary"></i></td>
                            <td><i class="fa fa-check-circle text-primary"></i></td>
                            <td class="table-striped-horizontal"><i class="fa fa-times-circle text-red"></i></td>
                            <td><i class="fa fa-check-circle text-primary"></i></td>
                            <td class="table-striped-horizontal"><i class="fa fa-times-circle text-red"></i></td>
                            <td><i class="fa fa-check-circle text-primary"></i></td>
                            <td class="table-striped-horizontal"><i class="fa fa-times-circle text-red"></i></td>
                            <td><i class="fa fa-times-circle text-red"></i></td>
                            <td class="table-striped-horizontal"><i class="fa fa-times-circle text-red"></i></td>
                            <td class="text-green text-center text-score">15/17</td>
                            <td class="text-green text-center text-score">80</td>
                          </tr>
                          <tr class="text-center">
                            <td class="text-left">Galih Pratama</td>
                            <td class="table-striped-horizontal"><i class="fa fa-check-circle text-primary"></i></td>
                            <td><i class="fa fa-times-circle text-red"></i></td>
                            <td class="table-striped-horizontal"><i class="fa fa-check-circle text-primary"></i></td>
                            <td><i class="fa fa-check-circle text-primary"></i></td>
                            <td class="table-striped-horizontal"><i class="fa fa-check-circle text-primary"></i></td>
                            <td><i class="fa fa-times-circle text-red"></i></td>
                            <td class="table-striped-horizontal"><i class="fa fa-check-circle text-primary"></i></td>
                            <td><i class="fa fa-times-circle text-red"></i></td>
                            <td class="table-striped-horizontal"><i class="fa fa-times-circle text-red"></i></td>
                            <td><i class="fa fa-check-circle text-primary"></i></td>
                            <td class="table-striped-horizontal"><i class="fa fa-check-circle text-primary"></i></td>
                            <td><i class="fa fa-times-circle text-red"></i></td>
                            <td class="table-striped-horizontal"><i class="fa fa-check-circle text-primary"></i></td>
                            <td><i class="fa fa-check-circle text-primary"></i></td>
                            <td class="table-striped-horizontal"><i class="fa fa-check-circle text-primary"></i></td>
                            <td><i class="fa fa-check-circle text-primary"></i></td>
                            <td class="table-striped-horizontal"><i class="fa fa-times-circle text-red"></i></td>
                            <td class="text-green text-center text-score">13/17</td>
                            <td class="text-green text-center text-score">70</td>
                          </tr>
                          <tr class="text-center">
                            <td class="text-left">Aqmarina Fauhan</td>
                            <td class="table-striped-horizontal"><i class="fa fa-check-circle text-primary"></i></td>
                            <td><i class="fa fa-times-circle text-red"></i></td>
                            <td class="table-striped-horizontal"><i class="fa fa-check-circle text-primary"></i></td>
                            <td><i class="fa fa-check-circle text-primary"></i></td>
                            <td class="table-striped-horizontal"><i class="fa fa-check-circle text-primary"></i></td>
                            <td><i class="fa fa-check-circle text-primary"></i></td>
                            <td class="table-striped-horizontal"><i class="fa fa-times-circle text-red"></i></td>
                            <td><i class="fa fa-check-circle text-primary"></i></td>
                            <td class="table-striped-horizontal"><i class="fa fa-times-circle text-red"></i></td>
                            <td><i class="fa fa-check-circle text-primary"></i></td>
                            <td class="table-striped-horizontal"><i class="fa fa-check-circle text-primary"></i></td>
                            <td><i class="fa fa-times-circle text-red"></i></td>
                            <td class="table-striped-horizontal"><i class="fa fa-times-circle text-red"></i></td>
                            <td><i class="fa fa-check-circle text-primary"></i></td>
                            <td class="table-striped-horizontal"><i class="fa fa-check-circle text-primary"></i></td>
                            <td><i class="fa fa-times-circle text-red"></i></td>
                            <td class="table-striped-horizontal"><i class="fa fa-check-circle text-primary"></i></td>
                            <td class="text-green text-center text-score">14/17</td>
                            <td class="text-green text-center text-score">75</td>
                          </tr>
                          <tr class="table-exam-header text-green">
                            <td></td>
                            <td class="table-striped-horizontal">3/3</td>
                            <td>2/3</td>
                            <td class="table-striped-horizontal">2/3</td>
                            <td>1/3</td>
                            <td class="table-striped-horizontal">1/3</td>
                            <td>2/3</td>
                            <td class="table-striped-horizontal">2/3</td>
                            <td>3/3</td>
                            <td class="table-striped-horizontal">3/3</td>
                            <td>3/3</td>
                            <td class="table-striped-horizontal">1/3</td>
                            <td>0/3</td>
                            <td class="table-striped-horizontal">2/3</td>
                            <td>2/3</td>
                            <td class="table-striped-horizontal">1/3</td>
                            <td>1/3</td>
                            <td class="table-striped-horizontal">3/3</td>
                            <td></td>
                            <td></td>
                          </tr>
                        </table>
                      </div>
                    </div><!-- /.analysis-table -->
                  </div><!-- /.tab-pane-content -->
                </div>
                <div class="visible-xs">
                  <strong>Informasi:</strong>
                  <br>
                  <i>Untuk anda yang mengakses lewat handphone/smartphone, silahkan geser tabel ke kiri
                  untuk melihat data selengkapnya</i>
                </div>
              </div>
            </div>
          </div>

          </div><!-- /.col-card -->
        </div><!-- /.col-md-12 -->
      </div><!-- /.row -->
    </div><!-- /.col-md-12 -->
  </div><!-- /.row -->
</div><!-- /.container-fluid -->
<script type="text/javascript" src="libraries/canvasjs-1.8.0/canvasjs.min.js"></script>
<script type="text/javascript" src="scripts/column_chart.js"></script>
