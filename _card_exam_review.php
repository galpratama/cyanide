<div class="col-md-4">
    <div class="col-card">
        <div class="section-card-essay-answer">
            <h4>
                <strong>Galih Pratama</strong>
                <br>
                XII-RPL-B
            </h4>
            <blockquote>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</blockquote>
        </div>
        <div class="section-card-essay-action">
            <div class="pull-left">
                <button href="#" class="btn btn-success btn-md">100%</button>
            </div>
            <div class="pull-right">
                <button
                    data-toggle="popover" data-trigger="click" data-placement="top" data-popover-content="#popoverData"
                    class="btn btn-default btn-md"><i class="fa fa-pencil"></i> Ubah Penilaian</button>
            </div>
            <!-- Content for Popover #1 -->
            <div id="popoverData" class="hidden">
                <div class="popover-body">
                    <input type="range" min="0" max="100" step="1" value="70">
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div><!-- /.col-card -->
</div><!-- /.col-md-8 -->

