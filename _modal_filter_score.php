<!-- Modal -->
<div class="modal fade" id="modalFilterScore" tabindex="-1" role="dialog" aria-labelledby="filterScoreLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-center" id="forgotPasswordLabel">Menampilkan Nilai</h4>
      </div>
      <div class="modal-body">
        <p class="text-center">
          Silahkan masukkan nama siswa yang ingin ditampilkan nilainya
        </p>
        <div class="form-group">
            <input type="text" id="name-typeahead" data-provide="typeahead" class="form-control input-lg input-pn input-pn-center" placeholder="Nama Siswa">
        </div>
        <p class="text-center">
          Atau pilih kelas
        </p>
        <div class="scrollable-table-list">
          <table class="table">
            <tbody>
              <?php
              for ($i=1; $i < 20; $i++) {
                ?>
                  <tr>
                    <td width="70%">Kelas XII-RPL-B Matematika</td>
                    <td width="30%" class="text-right">
                      <a href="dashboard.php?page=score" class="btn btn-pn-primary btn-xs">Tampilkan Nilai</a>
                    </td>
                  </tr>
                <?php
              }
              ?>
              </tbody>
            </table>

        </div>
      </div>
    </div>
  </div>
</div>
