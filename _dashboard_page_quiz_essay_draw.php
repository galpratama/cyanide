<div class="container-fluid">
  <div class="row">
    <div class="col-lg-12 container-soal-floating">
        <h2 class="pull-left">
          Kuis Essay Ujian Coba
          <br>
          <small>Waktu Pengerjaan : 120 Menit</small>
        </h2>
        <div class="pull-right countdown-right">
          <br>
          <div class="clock-timer" id="clock"></div>
          <a href="#" class="btn btn-pn-primary hide"><i class="fa fa-check"></i> Cek Jawaban</a>
        </div>
        <div class="clearfix"></div>
        <div class="row">
          <div class="col-md-2">
            <div class="row">
              <div class="switch-quiz-box">
                <div class="col-md-12">
                  <h4 class="hidden-xs hidden-sm">Daftar Soal</h4>
                  <div class="col-card">
                    <div class="list-soal-container">
                      <ul class="nav nav-pills">
                        <div class="hidden-md hidden-lg">
                          &nbsp;&nbsp;<i class="fa fa-arrow-up fa-2x"></i>
                        </div>
                        <li class="list-soal active"><a href="#step1" id="coba1" data-local="768" class="btn btn-soal" data-toggle="tab" data-step="1">1</a></li>
                        <li class="list-soal"><a href="#step2" id="coba2" data-local="769" class="btn btn-soal" data-toggle="tab" data-step="2" >2</a></li>
                        <li class="list-soal"><a href="#step2" id="coba2" data-local="769" class="btn btn-soal" data-toggle="tab" data-step="2" >3</a></li>
                        <li class="list-soal"><a href="#step2" id="coba2" data-local="769" class="btn btn-soal" data-toggle="tab" data-step="2" >4</a></li>
                        <li class="list-soal"><a href="#step2" id="coba2" data-local="769" class="btn btn-soal" data-toggle="tab" data-step="2" >5</a></li>
                        <li class="list-soal"><a href="#step2" id="coba2" data-local="769" class="btn btn-soal" data-toggle="tab" data-step="2" >6</a></li>
                        <li class="list-soal"><a href="#step2" id="coba2" data-local="769" class="btn btn-soal" data-toggle="tab" data-step="2" >7</a></li>
                        <li class="list-soal"><a href="#step2" id="coba2" data-local="769" class="btn btn-soal" data-toggle="tab" data-step="2" >8</a></li>
                        <li class="list-soal"><a href="#step2" id="coba2" data-local="769" class="btn btn-soal" data-toggle="tab" data-step="2" >9</a></li>
                        <li class="list-soal"><a href="#step2" id="coba2" data-local="769" class="btn btn-soal" data-toggle="tab" data-step="2" >10</a></li>
                        <li class="list-soal"><a href="#step2" id="coba2" data-local="769" class="btn btn-soal" data-toggle="tab" data-step="2" >7</a></li>
                        <li class="list-soal"><a href="#step2" id="coba2" data-local="769" class="btn btn-soal" data-toggle="tab" data-step="2" >8</a></li>
                        <li class="list-soal"><a href="#step2" id="coba2" data-local="769" class="btn btn-soal" data-toggle="tab" data-step="2" >9</a></li>
                        <li class="list-soal"><a href="#step2" id="coba2" data-local="769" class="btn btn-soal" data-toggle="tab" data-step="2" >10</a></li>
                        <div class="hidden-md hidden-lg">
                          &nbsp;&nbsp;<i class="fa fa-arrow-down fa-2x"></i>
                        </div>
                      </ul>
                    </div>
                  </div>
                  <div class="progress-container">
                    <div class="progress">
                      <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="8" style="width: 80%;">Soal 6 dari 18 Soal</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-10 col-canvas-mobile">
            <h4>Soal</h4>
            <div class="col-card col-canvas-mobile">
              <div class="col-md-12">
                <h3>
                  Soal 1
                </h3>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero asperiores, accusantium, tempore minus quidem incidunt illum vero corporis mollitia quis exercitationem ut, consectetur! Placeat nesciunt magni, modi consequatur tempora. Laborum mollitia at asperiores necessitatibus vel beatae, provident alias sit! Neque magnam ipsam architecto corrupti inventore dolores porro atque similique deleniti, assumenda doloremque non ipsa at, dolorem harum expedita voluptatem nobis reiciendis iure quibusdam laborum culpa. Sit cumque ea placeat nihil nobis vero eligendi eveniet perferendis nesciunt porro unde quibusdam animi quae, quo ipsa, numquam, commodi tempore laborum amet molestias labore totam! Dolorum at suscipit, pariatur, necessitatibus vel accusamus ex cupiditate!
                </p>
                <div class="well">
                  <i class="fa fa-question-circle"></i> <strong>Bantuan:</strong> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, quae!
                </div>
              </div>
              <div class="draw" style="width: 800px;">
                <div class="col-md-12">
                  <h3>Jawaban</h3>
                  <p>Silahkan menjawab pertanyaan ini menggunakan sketsa gambar</p>
                  <div class="drawing-toolbox">
                    <div class="drawing-tools">
                      <div class="btn-group">
                        <button id="drawing-mode" class="btn btn-info"><i class="fa fa-pencil"></i> Menulis</button>
                        <button id="selection-mode" class="btn btn-info"><i class="fa fa-mouse-pointer"></i> Seleksi</button>
                      </div>
                      <div class="btn-group">
                        <button id="undo-action" class="btn btn-default"><i class="fa fa-undo" ></i> Undo</button>
                        <button id="redo-action" class="btn btn-default"><i class="fa fa-repeat"></i> Redo</button>
                      </div>
                      <button type="button" class="btn btn-success" data-toggle="modal" data-target="#uploadImage">
                        <i class="fa fa-image"></i> Unggah
                      </button>
                      <span class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Tambah Bentuk
                          <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                          <li><a id="addCircle" href="javascript:void(0)">Lingkaran</a></li>
                          <li><a id="addRect" href="javascript:void(0)">Kotak</a></li>
                          <li><a id="addTriangle" href="javascript:void(0)">Segitiga</a></li>
                          <li><a id="addGrid" href="javascript:void(0)">Grid</a></li>
                        </ul>
                      </span>
                      <button data-toggle="collapse" class="btn btn-success" data-target="#add-text-options"><i class="fa fa-font"></i> Teks</button>
                      <button id="delete-element" class="btn btn-warning"><i class="fa fa-trash"></i> Hapus </button>
                      <button id="clear-canvas" class="btn btn-danger"><i class="fa fa-refresh"></i> Kosongkan</button>
                    </div>
                    <div id="uploadImage" class="modal fade" role="dialog">
                      <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Unggah Gambar</h4>
                          </div>
                          <div class="modal-body">
                            <p>Tambah gambar baru. Ukuran maksimal 800x500</p>
                            <input type="file" id="imgLoader" capture="camera">
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
                          </div>
                        </div>

                      </div>
                    </div>
                    <div id="previewCanvas" class="modal fade" role="dialog">
                      <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Pratinjau Hasil Jawaban</h4>
                          </div>
                          <div class="modal-body">
                            <div id="image-preview">
                              <div class="text-center">
                                <i class="fa fa-spin fa-spinner"></i>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
                          </div>
                        </div>

                      </div>
                    </div>
                    <div id="add-text-options" class="well col-md-9 collapse">
                      <div class="col-md-3">
                        <label for="">Tambah Teks</label><br>
                        <button id="add-text" class="btn btn-pn-primary btn-xs">Tambah Teks Baru</button>
                      </div>
                      <div class="col-md-4">
                        <label for="">Ganti Font</label><br>
                        <select id="font-toolbox">
                          <option>Arial</option>
                          <option>Tahoma</option>
                          <option>Calibri</option>
                          <option>Comic Sans</option>
                          <option>Times New Roman</option>
                        </select>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                    <div id="drawing-mode-options" class="well" style="display: none">
                      <h4>Mode Menulis</h4>
                      <p>Silahkan atur ukuran pensil dan warna pensil dibawah ini</p>
                      <div class="row">
                        <div class="col-md-2">
                          <label for="drawing-mode-selector">Mode:</label>
                          <br>
                          <select id="drawing-mode-selector">
                            <option>Pencil</option>
                            <option>Circle</option>
                            <option>Spray</option>
                            <option>Pattern</option>

                            <option>hline</option>
                            <option>vline</option>
                            <option>square</option>
                            <option>diamond</option>
                            <option>texture</option>
                          </select>
                        </div>
                        <div class="col-md-3">
                          <label for="drawing-line-width">Lebar pensil: </label>
                          <br>
                          <input type="range" value="30" min="0" max="150" id="drawing-line-width"><br>
                        </div>
                        <div class="col-md-3">
                          <label for="drawing-color">Warna pensil:</label><br>
                          <input type="color" value="#000" id="drawing-color">
                        </div>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="drawing-tools">
                      <div class="btn-group">
                        <button class="btn btn-default" id="zoomIn">
                          <i class="fa fa-search-plus"></i>
                        </button>
                        <button class="btn btn-default" id="zoomOut">
                          <i class="fa fa-search-minus"></i>
                        </button>
                      </div>
                      <div class="btn-group">
                        <button class="btn btn-default" id="goLeft">
                          <i class="fa fa-arrow-circle-left"></i>
                        </button>
                        <button class="btn btn-default" id="goRight">
                          <i class="fa fa-arrow-circle-right"></i>
                        </button>
                        <button class="btn btn-default" id="goUp">
                          <i class="fa fa-arrow-circle-up"></i>
                        </button>
                        <button class="btn btn-default" id="goDown">
                          <i class="fa fa-arrow-circle-down"></i>
                        </button>
                      </div>
                      <div class="btn-group">
                        <button type="button" class="btn btn-default" id="send-backward">
                          <i class="fa fa-arrow-down"></i> Taruh belakang
                        </button>
                        <button type="button" class="btn btn-default" id="bring-forward">
                          <i class="fa fa-arrow-up"></i> Taruh depan
                        </button>
                      </div>
                      <button class="btn btn-default" id="canvas2png" data-toggle="modal" data-target="#previewCanvas"><i class="fa fa-eye"></i> Pratinjau</button>
                      <input type="color" value="#000000" id="fill-toolbox" />
                    </div>
                  </div>
                  <div class="drawing-canvas">
                    <canvas id="c" width="800" height="1000" style="border:1px solid #aaa"></canvas>
                  </div>
                </div>
                <div class="clearfix"></div>
              </div>
            </div>
            <div class="row col-button">
              <div class="col-md-6">
                <a href="#" class="btn btn-warning btn-lg btn-block">
                  <i class="fa fa-chevron-left"></i> Sebelumnya
                </a>
                <br>
              </div>
              <div class="col-md-6">
                <a href="#" class="btn btn-pn-primary btn-lg btn-block">
                  Selanjutnya <i class="fa fa-chevron-right"></i>
                </a>
              </div>
            </div><!-- /.col-button -->
          </div>
        </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="libraries/canvasjs-1.8.0/canvasjs.min.js"></script>
<script type="text/javascript" src="libraries/jquery/jquery.min.js"></script>
<script type="text/javascript" src="libraries/countdown/jquery.countdown.js"></script>
<script src="libraries/fabric.min.js"></script>
<script type="text/javascript" src="scripts/quiz.js"></script>
