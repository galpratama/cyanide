<div class="col-md-4">
    <div class="col-card row">
      <div class="col-md-12">
        <div class="row">
          <div class="title-top-left">
              <h3>Tugas Kalkulus 1</h3>
              <p>
                  Matematika
                  <br>
                  XII - IPA - 1
              </p>
          </div>
        </div>
      </div>
      <div class="col-md-12">
        <br>
        <div class="chart-pie">
          <div id="chartPieContainer2"
               style="height: 200px; width: 100%;">
          </div>
          <div class="total" id="total-2">300</div>
        </div>
        <br>
      </div>
      <div class="col-md-12">
          <div class="row no-gutter row-accumulate">
              <div class="col-md-12 col-sm-12 col-xs-12 row-accumulate-middle">
                  <div class="accumulate-score accumulate-yellow">27</div>
                  <div class="accumulate-description">
                      Tugas
                      Belum Diperiksa
                  </div>
              </div>
              <div class="col-md-6 col-sm-6 col-xs-6 row-accumulate-left">
                  <div class="accumulate-score accumulate-red">27</div>
                  <div class="accumulate-description">
                      Siswa <br>
                      Sudah Selesai
                  </div>
              </div>
              <div class="col-md-6 col-sm-6 col-xs-6 row-accumulate-right">
                  <div class="accumulate-score accumulate-green">17</div>
                  <div class="accumulate-description">
                      Siswa <br>
                      Belum Selesai
                  </div>
              </div>
          </div>
      </div>
      <div class="col-md-12">
         <div class="row row-btn-integrated-card no-gutter">
             <br>
             <button type="button" class="btn btn-pn-primary btn-pn-card-integrated btn-lg btn-block" name="button">
                 SELENGKAPNYA
             </button>
         </div>
      </div>
      <div class="clearfix"></div>
    </div>
</div>
