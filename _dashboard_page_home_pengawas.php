<div class="container-fluid">
  <div class="row">
    <div class="col-lg-12">
      <div class="col-card">
        <div class="row">
          <div class="col-md-12 text-center">
            <h2>Ujian 1 Matematika</h2>
            <p>Waktu Pengerjaan: <strong>120 Menit</strong></p>
          </div>
          <div class="clearfix"></div>
          <hr>
          <?php

          for ($i=0; $i < 2; $i++) {
            ?>
            <div class="col-md-3 col-sm-4 col-pengawas-user">
              <figure>
                <img
                  src="https://scontent-amt2-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c170.0.683.683/13113846_177513025976068_1425170419_n.jpg?ig_cache_key=MTIzNjc0ODAyNDE5ODg5NDAxMA%3D%3D.2.c"
                  class="img-circle img-thumbnail"
                  width="150"
                  alt="">
              </figure>
              <hr>
              <div class="pengawas-information">
                <h4 class="pengawas-name">Jung Yerin</h4>
                <h4><span class="label label-warning"><i class="fa fa-times"></i> Belum Mulai</span></h4>
              </div>
              <hr>
            </div>
            <div class="col-md-3 col-sm-4 col-pengawas-user">
              <figure>
                <img
                  src="https://scontent-amt2-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c170.0.683.683/13113846_177513025976068_1425170419_n.jpg?ig_cache_key=MTIzNjc0ODAyNDE5ODg5NDAxMA%3D%3D.2.c"
                  class="img-circle img-thumbnail"
                  width="150"
                  alt="">
              </figure>
              <hr>
              <div class="pengawas-information">
                <h4 class="pengawas-name">Jung Yerin Namanya Panjang</h4>
                <h4><span class="label label-info"><i class="fa fa-spinner fa-spin"></i> Sedang Mengerjakan</span></h4>
                <button type="button" name="button" class="btn btn-danger btn-sm"><i class="fa fa-power-off"></i> Freeze</button>
              </div>
              <hr>
            </div>
            <div class="col-md-3 col-sm-4 col-pengawas-user">
              <figure>
                <img
                  src="https://scontent-amt2-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c170.0.683.683/13113846_177513025976068_1425170419_n.jpg?ig_cache_key=MTIzNjc0ODAyNDE5ODg5NDAxMA%3D%3D.2.c"
                  class="img-circle img-thumbnail img-pengawas-freeze"
                  width="150"
                  alt="">
              </figure>
              <hr>
              <div class="pengawas-information">
                <h4 class="pengawas-name">Jung Yerin</h4>
                <h4><span class="label label-info"><i class="fa fa-spinner fa-spin"></i> Sedang Mengerjakan</span></h4>
                <button type="button" name="button" class="btn btn-primary btn-sm"><i class="fa fa-power-off"></i> Buka Freeze</button>
              </div>
              <hr>
            </div>
            <div class="col-md-3 col-sm-4 col-pengawas-user">
              <figure>
                <img
                  src="https://scontent-amt2-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c170.0.683.683/13113846_177513025976068_1425170419_n.jpg?ig_cache_key=MTIzNjc0ODAyNDE5ODg5NDAxMA%3D%3D.2.c"
                  class="img-circle img-thumbnail"
                  width="150"
                  alt="">
              </figure>
              <hr>
              <div class="pengawas-information">
                <h4 class="pengawas-name">Jung Yerin Namanya Panjang</h4>
                <h4><span class="label label-success"><i class="fa fa-check"></i> Sudah Selesai</span></h4>
              </div>
              <hr>
            </div>
            <?php
          }
          ?>
        </div>
      </div><!-- /.col-card -->
    </div><!-- /.col-lg-12 -->
  </div><!-- /.row -->
</div>

<script type="text/javascript" src="libraries/canvasjs-1.8.0/canvasjs.min.js"></script>
<script type="text/javascript" src="scripts/pie_chart.js"></script>
