<div class="container-fluid">
  <div class="row">
    <div class="col-lg-12">
      <div class="col-card">
        <div class="row row-student-overview">
          <h3>Selamat Datang</h3>
          <p>
            <strong>Berikut ini merupakan informasi singkat mengenai ujian dan tugas anda</strong>
          </p>
          <br>
          <div class="col-lg-3 col-sm-6 col-xs-6">
            <div class="overview-number overview-green">
              12
            </div>
            <div class="overview-description">
              Tugas Selesai
            </div>
          </div><!-- /.col-lg-3 col-sm-6 col-xs-6 -->
          <div class="col-lg-3 col-sm-6 col-xs-6">
            <div class="overview-number overview-red">
              6
            </div>
            <div class="overview-description">
              Tugas Belum Selesai
            </div>
          </div><!-- /.col-lg-3 col-sm-6 col-xs-6 -->
          <div class="col-lg-3 col-sm-6 col-xs-6">
            <div class="overview-number overview-green">
              2
            </div>
            <div class="overview-description">
              Ujian Selesai
            </div>
          </div><!-- /.col-lg-3 col-sm-6 col-xs-6 -->
          <div class="col-lg-3 col-sm-6 col-xs-6">
            <div class="overview-number overview-yellow">
              0
            </div>
            <div class="overview-description">
              Ujian Mendatang
            </div>
          </div><!-- /.col-lg-3 col-sm-6 col-xs-6 -->
        </div><!-- /.row -->
      </div><!-- /.col-card -->
    </div><!-- /.col-lg-12 -->
  </div><!-- /.row -->
  <div class="row">
    <div class="col-lg-6">
        <h3>Informasi Ujian</h3>
        <div>
            <div class="news-box">
                <div class="news-card-container">
                    <div class="news-card">
                        <div class="news-card-title">
                            <h3>
                                Ulangan Harian Matematika XII IPA
                            </h3>
                            <p>
                              Lorem ipsum dolor sit amet, consectetur adipisicing.
                            </p>
                        </div>
                        <div class="news-card-content">
                            <div class="news-card-schedule">
                                <ul>
                                    <li class="schedule-date schedule-date-near">
                                        <i class="icon-pn-calendar"></i> 25 April 2016 (2 hari lagi)
                                    </li>
                                    <li class="schedule-time">
                                        <i class="icon-pn-clock"></i> 10:15 - 12:00
                                    </li>
                                    <li class="schedule-location">
                                        <i class="icon-pn-round"></i> Ruang XII IPA 1
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-card">
                        <div class="news-card-title">
                            <h3>
                                Ulangan Harian Sosiologi XII IPA
                            </h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos alias rem mollitia nihil doloribus dolores quod amet!</p>
                        </div>
                        <div class="news-card-content">
                            <div class="news-card-schedule">
                                <ul>
                                    <li class="schedule-date schedule-date-far">
                                        <i class="icon-pn-calendar"></i> 27 April 2016 (2 hari lagi)
                                    </li>
                                    <li class="schedule-time">
                                        <i class="icon-pn-clock"></i> 10:15 - 12:00
                                    </li>
                                    <li class="schedule-location">
                                        <i class="icon-pn-round"></i> Ruang XII IPA 1
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-card">
                        <div class="news-card-title">
                            <h3>
                                Ulangan Harian Fisika XII IPA
                            </h3>
                            <p>
                              Lorem ipsum dolor sit amet.
                            </p>
                        </div>
                        <div class="news-card-content">
                            <div class="news-card-schedule">
                                <ul>
                                    <li class="schedule-date schedule-date-far">
                                        <i class="icon-pn-calendar"></i> 29 April 2016 (2 hari lagi)
                                    </li>
                                    <li class="schedule-time">
                                        <i class="icon-pn-clock"></i> 10:15 - 12:00
                                    </li>
                                    <li class="schedule-location">
                                        <i class="icon-pn-round"></i> Ruang XII IPA 1
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6">
            <h3>Tugas Terbaru</h3>
            <div>
                <div class="news-box">
                    <div class="news-card-container">
                        <div class="news-card">
                            <div class="news-card-title">
                                <h3>
                                    Tugas Kimia
                                </h3>
                                <p>
                                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iure provident magni a numquam in, culpa atque dolor, officia sunt nobis.
                                </p>
                            </div>
                            <div class="news-card-content">
                                <div class="news-card-schedule">
                                    <ul>
                                        <li class="schedule-date schedule-date-far">
                                            <i class="icon-pn-calendar"></i> Batas Pengumpulan : 27 Juni 2016
                                        </li>
                                        <li class="schedule-time">
                                            <i class="icon-pn-clock"></i> 12:00
                                        </li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="news-card">
                            <div class="news-card-title">
                                <h3>
                                    Tugas Sosiologi
                                </h3>
                                <p>
                                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis, dolores.
                                </p>
                            </div>
                            <div class="news-card-content">
                                <div class="news-card-schedule">
                                    <ul>
                                        <li class="schedule-date schedule-date-far">
                                            <i class="icon-pn-calendar"></i> Batas Pengumpulan : 27 Juni 2016
                                        </li>
                                        <li class="schedule-time">
                                            <i class="icon-pn-clock"></i> 12:00
                                        </li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="news-card">
                            <div class="news-card-title">
                                <h3>
                                    Tugas Matematika
                                </h3>
                                <p>
                                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum nostrum, laboriosam ullam itaque unde eos?
                                </p>
                            </div>
                            <div class="news-card-content">
                                <div class="news-card-schedule">
                                    <ul>
                                        <li class="schedule-date schedule-date-far">
                                            <i class="icon-pn-calendar"></i> Batas Pengumpulan : 27 Juni 2016
                                        </li>
                                        <li class="schedule-time">
                                            <i class="icon-pn-clock"></i> 12:00
                                        </li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
            </div>
      </div>
  </div>
</div>

<script type="text/javascript" src="libraries/canvasjs-1.8.0/canvasjs.min.js"></script>
<script type="text/javascript" src="scripts/pie_chart.js"></script>
