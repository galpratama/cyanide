<div class="container-fluid">
  <div class="row">
      <?php include '_breadcrumb_course_student_detail_video.php'; ?>
      <br><br>
      <div class="lesson-player">
          <div class="">
              <div class="section-lesson-player">
                  <div class="col-md-8 section-lesson-player-left">
                      <div class="course-content">
                          <ul class="nav-card">
                              <li class="active"><a data-toggle="pill" href="#about">Informasi</a></li>
                              <li><a data-toggle="pill" href="#notes">Catatan Pengajar</a></li>
                              <li><a data-toggle="pill" href="#downloads">Berkas Pendukung</a></li>
                          </ul>

                          <div class="tab-content">
                              <div id="about" class="tab-pane col-card card-regular active">
                                  <p>
                                      In this stage we show you how to install WordPress on your computer using the popular free software DesktopServer. This lets you practice building sites with WordPress right on your computer without having to purchase a hosting account.
                                  </p>

                                  <p>
                                      In this course we will go over the pros and cons of running WordPress on your computer, or locally, as it’s called as well as show how to setup DesktopServer on a Windows and Mac. Once we have DesktopServer installed we’ll go over how we can use it to create WordPress sites with just a few clicks.
                                  </p>

                                  <p>
                                      He has an extensive background in web design and WordPress education, having taught and help run college and high school web programs. You can often find him attending and speaking at local WordCamp meetups.
                                  </p>

                                  <p>
                                      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab accusamus ad aliquam aperiam aut autem beatae consequatur cumque delectus deleniti, deserunt dolorem dolores earum expedita harum inventore ipsam laboriosam non nostrum nulla officia omnis provident quae quam quisquam sapiente sunt tenetur ut velit voluptate. A aliquam amet doloremque, dolorum eveniet ipsum laboriosam nisi provident quod sed. Alias asperiores atque consectetur consequuntur distinctio dolore eius expedita fuga fugiat inventore, iure laborum laudantium libero magni maxime, nam nulla quaerat reiciendis rem sapiente sint sit soluta veniam vero voluptas! Accusantium ad laudantium veniam.
                                  </p>
                                  <p>
                                      In this stage we show you how to install WordPress on your computer using the popular free software DesktopServer. This lets you practice building sites with WordPress right on your computer without having to purchase a hosting account.
                                  </p>

                                  <p>
                                      In this course we will go over the pros and cons of running WordPress on your computer, or locally, as it’s called as well as show how to setup DesktopServer on a Windows and Mac. Once we have DesktopServer installed we’ll go over how we can use it to create WordPress sites with just a few clicks.
                                  </p>

                                  <p>
                                      He has an extensive background in web design and WordPress education, having taught and help run college and high school web programs. You can often find him attending and speaking at local WordCamp meetups.
                                  </p>

                                  <p>
                                      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab accusamus ad aliquam aperiam aut autem beatae consequatur cumque delectus deleniti, deserunt dolorem dolores earum expedita harum inventore ipsam laboriosam non nostrum nulla officia omnis provident quae quam quisquam sapiente sunt tenetur ut velit voluptate. A aliquam amet doloremque, dolorum eveniet ipsum laboriosam nisi provident quod sed. Alias asperiores atque consectetur consequuntur distinctio dolore eius expedita fuga fugiat inventore, iure laborum laudantium libero magni maxime, nam nulla quaerat reiciendis rem sapiente sint sit soluta veniam vero voluptas! Accusantium ad laudantium veniam.
                                  </p>
                              </div>
                              <div id="notes" class="tab-pane col-card card-regular">
                                  <p>
                                      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus corporis deserunt doloribus ex exercitationem facere fuga id incidunt magni natus nobis nostrum obcaecati officiis, optio quasi quo tempore vel voluptates!
                                  </p>
                              </div>
                              <div id="downloads" class="tab-pane col-card card-regular">
                                  <ul class="course-list">
                                      <li>
                                          <i class="fa fa-file-zip-o"></i> Source Code
                                          <small class="pull-right">
                                              <a href="#"><i class="fa fa-download"></i> Unduh</a>
                                          </small>
                                      </li>
                                      <li>
                                          <i class="fa fa-file-zip-o"></i> Source Code
                                          <small class="pull-right">
                                              <a href="#"><i class="fa fa-download"></i> Unduh</a>
                                          </small>
                                      </li>
                                      <li>
                                          <i class="fa fa-file-zip-o"></i> Source Code
                                          <small class="pull-right">
                                              <a href="#"><i class="fa fa-download"></i> Unduh</a>
                                          </small>
                                      </li>
                                      <li>
                                          <i class="fa fa-file-zip-o"></i> Source Code
                                          <small class="pull-right">
                                              <a href="#"><i class="fa fa-download"></i> Unduh</a>
                                          </small>
                                      </li>
                                      <li>
                                          <i class="fa fa-file-zip-o"></i> Source Code
                                          <small class="pull-right">
                                              <a href="#"><i class="fa fa-download"></i> Unduh</a>
                                          </small>
                                      </li>
                                      <li>
                                          <i class="fa fa-file-zip-o"></i> Source Code
                                          <small class="pull-right">
                                              <a href="#"><i class="fa fa-download"></i> Unduh</a>
                                          </small>
                                      </li>
                                      <li>
                                          <i class="fa fa-file-zip-o"></i> Source Code
                                          <small class="pull-right">
                                              <a href="#"><i class="fa fa-download"></i> Unduh</a>
                                          </small>
                                      </li>
                                  </ul>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="col-md-4 section-lesson-player-right">
                      <div class="col-card card-regular card-lesson-list">
                          <ul class="course-list">
                              <li class="text-muted">
                                  <a href="dashboard.php?page=course-student-detail-video">
                                      <i class="fa fa-play-circle-o"></i> Instalasi Project
                                      <small class="pull-right text-muted">
                                          3:50
                                      </small>
                                  </a>
                              </li>
                              <li class="text-muted">
                                  <a href="dashboard.php?page=course-student-detail-video">
                                      <i class="fa fa-file-pdf-o"></i> Pengenalan Bahasa Korea
                                      <small class="pull-right text-muted">
                                          3:50
                                      </small>
                                  </a>
                              </li>
                              <li class="text-strong">
                                  <a href="dashboard.php?page=course-student-detail-video">
                                      <i class="fa fa-file-text-o"></i> Navigation Bar
                                      <small class="pull-right text-muted">
                                          3:50
                                      </small>
                                  </a>
                              </li>
                              <li>
                                  <a href="dashboard.php?page=course-student-detail-video">
                                      <i class="fa fa-play-circle-o"></i> Selesaikan Project
                                      <small class="pull-right text-muted">
                                          3:50
                                      </small>
                                  </a>
                              </li>
                              <li>
                                  <a href="dashboard.php?page=course-student-detail-video">
                                      <i class="fa fa-play-circle-o"></i> Refactor Code
                                      <small class="pull-right text-muted">
                                          3:50
                                      </small>
                                  </a>
                              </li>
                              <li>
                                  <a href="dashboard.php?page=course-student-detail-video">
                                      <i class="fa fa-play-circle-o"></i> Selesaikan Project
                                      <small class="pull-right text-muted">
                                          3:50
                                      </small>
                                  </a>
                              </li>
                              <li>
                                  <a href="dashboard.php?page=course-student-detail-video">
                                      <i class="fa fa-play-circle-o"></i> Refactor Code
                                      <small class="pull-right text-muted">
                                          3:50
                                      </small>
                                  </a>
                              </li>
                              <li>
                                  <a href="dashboard.php?page=course-student-detail-video">
                                      <i class="fa fa-play-circle-o"></i> Selesaikan Project
                                      <small class="pull-right text-muted">
                                          3:50
                                      </small>
                                  </a>
                              </li>
                              <li>
                                  <a href="dashboard.php?page=course-student-detail-video">
                                      <i class="fa fa-play-circle-o"></i> Refactor Code
                                      <small class="pull-right text-muted">
                                          3:50
                                      </small>
                                  </a>
                              </li>
                              <li>
                                  <a href="dashboard.php?page=course-student-detail-video">
                                      <i class="fa fa-play-circle-o"></i> Selesaikan Project
                                      <small class="pull-right text-muted">
                                          3:50
                                      </small>
                                  </a>
                              </li>
                              <li>
                                  <a href="dashboard.php?page=course-student-detail-video">
                                      <i class="fa fa-play-circle-o"></i> Refactor Code
                                      <small class="pull-right text-muted">
                                          3:50
                                      </small>
                                  </a>
                              </li>
                          </ul>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <br><br>
      <div class="lesson-detail">
          <div>
              <div>
                  <div class="col-md-8">

                  </div>
              </div>
          </div>
      </div>
  </div>
</div>
<script type="text/javascript" src="libraries/canvasjs-1.8.0/canvasjs.min.js"></script>
<script type="text/javascript" src="libraries/jquery/jquery.min.js"></script>
<script type="text/javascript" src="scripts/course_detail.js"></script>
<script type="text/javascript" src="libraries/pplayer/js/jquery.pplayer.js"></script>
<script>
    $("#youtube-video").pPlayer({
        youtubeVideoId: "ccBh67LDqOI",
        autoplay: 0,
        hd: 0,
        origin: "https://belajarkoding.com",
        features: [ "playpause", "progress", "quality", "timer", "mute", "fullscreen" ]
    });
</script>
