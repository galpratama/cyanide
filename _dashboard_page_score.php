<div class="container-fluid">
  <div class="row">
    <?php include '_breadcrumb_score.php'; ?>
    <div class="col-lg-12">
        <h3>Nilai Matematika XII-IPA-1
          <small class="hidden-xs">Data Nilai</small>
          <div class="pull-right">
            <a href="dashboard.php?page=exam-add" class="btn btn-sm btn-pn-primary btn-pn-round">
              <span class="hidden-sm hidden-xs"><i class="fa fa-download"></i> Unduh File .XLS</span>
              <span class="hidden-md hidden-lg"><i class="fa fa-download"></i></span>
            </a>
          </div>
        </h3>
        <div class="row">
          <div class="col-md-12">
            <div class="col-card">
              <div>
                <h3>
                  KKM: 80
                  <a href="dashboard.php?page=exam-add" class="btn btn-xs btn-primary">
                    <i class="fa fa-pencil"></i> Edit KKM
                  </a>
                </h3>
              </div>
              <div class="table-responsive">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>No.</th>
                      <th>Nama</th>
                      <th>KD 3.1</th>
                      <th>KD 3.1</th>
                      <th>KD 3.1</th>
                      <th>KD 3.1</th>
                      <th>KD 3.1</th>
                      <th>UTS</th>
                      <th>UAS</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php include '_table_score.php'; ?>
                    <?php include '_table_score.php'; ?>
                    <?php include '_table_score.php'; ?>
                    <?php include '_table_score.php'; ?>
                    <?php include '_table_score.php'; ?>
                    <?php include '_table_score.php'; ?>
                    <?php include '_table_score.php'; ?>
                    <?php include '_table_score.php'; ?>
                    <?php include '_table_score.php'; ?>
                    <?php include '_table_score.php'; ?>
                  </tbody>
                </table>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript" src="libraries/canvasjs-1.8.0/canvasjs.min.js"></script>
<script type="text/javascript" src="scripts/pie_chart.js"></script>
