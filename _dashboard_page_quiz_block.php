<div class="container-fluid">
  <div class="row">
    <div class="col-lg-12 container-soal-floating">
      <!-- The overlay -->
      <div id="overlay" class="overlay">
        <div class="overlay-content text-center">
          <img src="images/cross.png" alt="" width="150px">
          <h1>Ujian ini dibatasi</h1>
          <p>Silakan masukan kode untuk membuka ujian ini</p>
          <div class="container">
            <div class="row">
              <div class="col-md-4 col-md-offset-4">
                <input type="email" class="form-control input-lg input-pn-center" placeholder="Kode Akses" style="margin-bottom: 5px;">
                <button class="btn btn-pn-primary btn-block">Buka Akses</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="libraries/canvasjs-1.8.0/canvasjs.min.js"></script>
<script type="text/javascript" src="libraries/jquery/jquery.min.js"></script>
<script type="text/javascript" src="libraries/countdown/jquery.countdown.js"></script>
<script type="text/javascript" src="scripts/quiz.js"></script>
