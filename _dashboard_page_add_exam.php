<div class="container-fluid">
  <div class="row">
    <?php include '_breadcrumb_exam_add.php'; ?>
    <div class="col-lg-12">
      <h3>Membuat Ujian Baru</small></h3>
      <div class="row">
        <div class="col-md-12">
          <div class="col-card">
            <h4>Informasi Umum</h4>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="namaUjian">Nama Ujian</label>
                  <input type="text" class="form-control input-pn input-lg" id="namaUjian" placeholder="Isi nama ujian disini (contoh: Ujian Bahasa Indonesia) …">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="namaMateri">Materi Yang Diujikan</label>
                  <input type="text" class="form-control input-pn input-lg" id="namaMateri" placeholder="Isi materi yang diujikan (contoh: KD 3.1 Membaca) …">
                </div>
              </div>

            </div>
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label for="namaPelajaran">Nama Mata Pelajaran</label>
                  <select name="namaPelajaran" class="selectpicker form-control" data-style="btn-default input-lg" data-live-search="true">
                    <option>Hot Dog, Fries and a Soda</option>
                    <option>Burger, Shake and a Smile</option>
                    <option>Sugar, Spice and all things nice</option>
                  </select>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label for="semester">Semester</label>
                  <div class="btn-group btn-group-justified" data-toggle="buttons">
                    <label class="btn btn-info btn-lg active">
                      <input type="radio" name="semester" id="option1" autocomplete="off" checked> 1
                    </label>
                    <label class="btn btn-info btn-lg ">
                      <input type="radio" name="semester" id="option2" autocomplete="off"> 2
                    </label>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="jenisUjian">Jenis Diujikan</label>
                  <div class="btn-group btn-group-justified" data-toggle="buttons">
                    <label class="btn btn-info btn-lg active">
                      <input type="radio" name="jenisUjian" id="option1" autocomplete="off" checked> UH
                    </label>
                    <label class="btn btn-info btn-lg ">
                      <input type="radio" name="jenisUjian" id="option2" autocomplete="off"> UTS
                    </label>
                    <label class="btn btn-info btn-lg ">
                      <input type="radio" name="jenisUjian" id="option3" autocomplete="off"> UAS
                    </label>
                  </div>
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label for="lamaUjian">Lama Ujian (menit)</label>
                  <input type="email" class="form-control input-pn input-lg" id="lamaUjian" placeholder="Contoh: 120">
                </div>
              </div>
            </div>
            <h4>Pelaksanaan Ujian</h4>
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label for="acakSoal">Acak Soal</label>
                  <div class="btn-group btn-group-justified" data-toggle="buttons">
                    <label class="btn btn-info btn-lg active">
                      <input type="radio" name="acakSoal" id="option1" autocomplete="off" checked> Ya
                    </label>
                    <label class="btn btn-info btn-lg ">
                      <input type="radio" name="acakSoal" id="option2" autocomplete="off"> Tidak
                    </label>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label for="acakJawaban">Acak Jawaban</label>
                  <div class="btn-group btn-group-justified" data-toggle="buttons">
                    <label class="btn btn-info btn-lg active">
                      <input type="radio" name="acakJawaban" id="option1" autocomplete="off" checked> Ya
                    </label>
                    <label class="btn btn-info btn-lg ">
                      <input type="radio" name="acakJawaban" id="option2" autocomplete="off"> Tidak
                    </label>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="tipeSoal">Tipe Soal</label>
                  <div class="btn-group btn-group-justified" data-toggle="buttons">
                    <label class="btn btn-info btn-lg active">
                      <input type="radio" name="tipeSoal" id="option1" autocomplete="off" checked> Essay
                    </label>
                    <label class="btn btn-info btn-lg ">
                      <input type="radio" name="tipeSoal" id="option2" autocomplete="off"> PG
                    </label>
                    <label class="btn btn-info btn-lg ">
                      <input type="radio" name="tipeSoal" id="option3" autocomplete="off"> Uraian
                    </label>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label for="menampilkanNilai">Menampilkan Nilai Setelah Ujian</label>
                  <div class="btn-group btn-group-justified" data-toggle="buttons">
                    <label class="btn btn-info btn-lg active">
                      <input type="radio" name="menampilkanNilai" id="option1" autocomplete="off" checked> Ya
                    </label>
                    <label class="btn btn-info btn-lg ">
                      <input type="radio" name="menampilkanNilai" id="option2" autocomplete="off"> Tidak
                    </label>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="masukRekap">Masuk Rekap Nilai</label>
                  <div class="btn-group btn-group-justified" data-toggle="buttons">
                    <label class="btn btn-info btn-lg active">
                      <input type="radio" name="masukRekap" id="option1" autocomplete="off" checked> Ya
                    </label>
                    <label class="btn btn-info btn-lg ">
                      <input type="radio" name="masukRekap" id="option2" autocomplete="off"> Tidak
                    </label>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="lamaUjian">Banyaknya Percobaan Yang Dibolehkan</label>
                  <input type="email" class="form-control input-pn input-lg" id="lamaUjian" placeholder="Contoh: 3">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <button type="button" class="btn btn-pn-primary btn-lg btn-pn-round btn-block next-step">Buat Ujian</button>
              </div>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>

<script type="text/javascript" src="libraries/canvasjs-1.8.0/canvasjs.min.js"></script>
<script type="text/javascript" src="scripts/pie_chart.js"></script>
