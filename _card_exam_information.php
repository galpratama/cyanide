<div class="col-card card-exam-switch">
  <div class="pull-left">
    <h4>
      Status Ujian <span class="label label-danger">Nonaktif</span>
    </h4>
  </div><!-- /.pull-left -->
  <div class="pull-right">
    <div class="pn-switch">
        <input type="checkbox" name="switch1" class="pn-switch-checkbox" id="switch1">
        <label class="pn-switch-label" for="switch1"></label>
    </div>
  </div><!-- /.pull-right -->
  <div class="clearfix"></div><!-- /.clearfix -->
</div><!-- /.col-card -->
<div class="col-card">
  <table class="table table-hover table-condensed">
    <thead>
        <tr>
          <th>
            Informasi
          </th>
          <th>
            Detail
          </th>
        </tr>
      </thead>
    <tbody>
      <tr>
        <td>
          Waktu Pengerjaan
        </td>
        <td>
          120 Menit
        </td>
      </tr>
      <tr>
        <td>
          Total Pertanyan
        </td>
        <td>
          50
        </td>
      </tr>
      <tr>
        <td>
          Acak Soal
        </td>
        <td>
          Ya
        </td>
      </tr>
      <tr>
        <td>
          Percobaan Kuis
        </td>
        <td>
          1 Kali
        </td>
      </tr>
      <tr>
        <td>
          Status
        </td>
        <td>
          Tampil
        </td>
      </tr>
      <tr>
        <td>
          Kode Pembuka
        </td>
        <td>
          <code>c4dmF</code>
        </td>
      </tr>
    </tbody>
  </table>
  <!-- Single button -->
  <div class="btn-group btn-block dropup">
    <button type="button" class="btn btn-pn-primary btn-lg btn-block dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class="fa fa-gear"></i> Aksi Ujian <i class="fa fa-caret-up"></i>
    </button>
    <ul class="dropdown-menu btn-block">
      <li><a href="#"><i class="fa fa-pencil"></i> Sunting Informasi Ujian</a></li>
      <li><a href="#"><i class="fa fa-eye"></i> Pratinjau Ujian</a></li>
      <li><a href="#"><i class="fa fa-download"></i> Unduh Nilai Ujian</a></li>
      <li role="separator" class="divider"></li>
      <li><a href="#"><i class="fa fa-copy"></i> Salin Ujian</a></li>
    </ul>
  </div>
</div><!-- /.col-card -->
