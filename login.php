<!DOCTYPE html>
<html>
    <head>
        <?php include '_head.php'; ?>
    </head>
    <body>
        <div class="login-page-container">
          <div class="login-page">
              <div class="container">
                  <div class="col-md-8 col-md-push-2">
                      <div class="login-box">
                        <div class="login-box-header">
                            <h1>Edubox</h1>
                            <div class="school-name">SMAN 12 Bandung</div>
                        </div>
                        <div class="login-box-form">
                            <h2 class="login-heading">Masuk ke Sistem</h2>
                            <div class="col-md-8 col-md-push-2">
                                <div class="login-form">
                                    <form action="dashboard.php" method="get">
                                        <input type="hidden" name="page" value="home">
                                        <div class="form-group">
                                            <input type="email" class="form-control input-lg input-pn input-pn-login input-pn-center" placeholder="NIS, NIP, atau Email anda">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control input-lg input-pn input-pn-login input-pn-center" placeholder="Kata Sandi">
                                        </div>
                                        <button type="submit" class="btn btn-pn-primary btn-pn-round btn-lg btn-block">MASUK</button>
                                    </form>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <a href="#" data-toggle="modal" data-target="#modalForgotPassword">Lupa Password?</a>
                            <?php include '_modal_forgot_password.php'; ?>
                        </div>
                        <div class="news-box">
                            <h2 class="login-heading">Informasi Aktivitas Terbaru</h2>
                            <div class="news-card-container">
                                <div class="news-card">
                                    <div class="news-card-title">
                                        <h3>
                                            Persiapan UAS
                                        </h3>
                                    </div>
                                    <div class="news-card-content">
                                        <div class="news-card-text">
                                            <p>
                                                “Siswa kelas X dan XII diharapkan untuk segera mempersiapkan diri untuk
                                                melaksanakan UAS karena waktu pelaksanaan sebentar lagi.”
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="news-card">
                                    <div class="news-card-title">
                                        <h3>
                                            Ulangan Harian Matematika XII IPA
                                        </h3>
                                    </div>
                                    <div class="news-card-content">
                                        <div class="news-card-schedule">
                                            <ul>
                                                <li class="schedule-date schedule-date-near">
                                                    <i class="icon-pn-calendar"></i> 25 April 2016 (2 hari lagi)
                                                </li>
                                                <li class="schedule-time">
                                                    <i class="icon-pn-clock"></i> Pukul 10:15 - 12:00
                                                </li>
                                                <li class="schedule-location">
                                                    <i class="icon-pn-round"></i> Ruang XII IPA 1
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="news-card">
                                    <div class="news-card-title">
                                        <h3>
                                            Tugas Besar Sosiologi
                                        </h3>
                                    </div>
                                    <div class="news-card-content">
                                        <div class="news-card-schedule">
                                            <ul>
                                                <li class="schedule-date schedule-date-far">
                                                    <i class="icon-pn-calendar"></i> Batas Pengumpulan : 20 Mei 2016
                                                </li>
                                                <li class="schedule-time">
                                                    <i class="icon-pn-clock"></i> Pukul 12:00
                                                </li>
                                                <li class="schedule-location">
                                                    <i class="icon-pn-round"></i> Kelas XI IPS
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                      </div>
                  </div>
              </div>
          </div>
        </div>
    </body>
    <?php include '_foot.php'; ?>
</html>
