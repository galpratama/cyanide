<tr>
  <th class="text-center">1</th>
  <td class="collapsible-row-container">
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vero, corporis.
  </td>
  <td>
    <span class="label label-success">Aktif</span>
  </td>
  <td>
    <span class="text-red text-bold">80%</span>
  </td>
  <td>
    <span class="text-green text-bold">20%</span>
  </td>
  <td>
    Pendidikan Pancasila Dan Kewarganegaraan
  </td>
  <td>
    X IPA 5
  </td>
  <td>
    Galih Pratama
  </td>
  <td>
    <div class="btn-group">
      <a href="dashboard.php?page=edit-exam" class="btn btn-success btn-xs">
        <i class="fa fa-eye"></i>
      </a>
      <a href="dashboard.php?page=edit-exam" class="btn btn-primary btn-xs">
        <i class="fa fa-pencil"></i>
      </a>
    </div>
  </td>
</tr>
