<div class="container-fluid">
  <div class="row">
    <?php include '_breadcrumb_exam.php'; ?>
    <div class="col-lg-12">
        <h3>Ujian
          <small class="hidden-xs">Daftar Ujian Anda</small>
          <span class="dropdown">
            <button class="btn btn-default btn-sm dropdown-toggle" type="button" id="dropdownUjian" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
              Terbaru
              <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownUjian">
              <li><a href="#">Terbaru</a></li>
              <li><a href="#">Aktif</a></li>
              <li><a href="#">Tidak Aktif</a></li>
            </ul>
          </span>
          <div class="pull-right">
            <a href="dashboard.php?page=exam-add" class="btn btn-sm btn-pn-primary btn-pn-round">
              <span class="hidden-sm hidden-xs"><i class="fa fa-plus-circle"></i> TAMBAH UJIAN</span>
              <span class="hidden-md hidden-lg"><i class="fa fa-plus-circle"></i></span>
            </a>
          </div>
        </h3>
        <div class="row">
          <?php include '_card_exam.php'; ?>
          <?php include '_card_blank_add_exam.php'; ?>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript" src="libraries/canvasjs-1.8.0/canvasjs.min.js"></script>
<script type="text/javascript" src="scripts/pie_chart.js"></script>
