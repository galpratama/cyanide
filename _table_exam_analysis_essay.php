<div class="table-responsive">
  <table class="table table-hover table-bordered ">
    <tr class="table-exam-header">
      <th rowspan="2">Nama Siswa</th>
      <th colspan="17" class="text-center">Nomor Soal (urut dari mudah ke sukar)</th>
      <th rowspan="2">Total</th>
      <th rowspan="2">Nilai</th>
    </tr>
    <tr class="table-exam-header">
      <td class="table-striped-horizontal">13</td>
      <td>2</td>
      <td class="table-striped-horizontal">7</td>
      <td>6</td>
      <td class="table-striped-horizontal">9</td>
      <td>10</td>
      <td class="table-striped-horizontal">21</td>
      <td>23</td>
      <td class="table-striped-horizontal">1</td>
      <td>4</td>
      <td class="table-striped-horizontal">5</td>
      <td>8</td>
      <td class="table-striped-horizontal">29</td>
      <td>12</td>
      <td class="table-striped-horizontal">19</td>
      <td>17</td>
      <td class="table-striped-horizontal">18</td>
    </tr>
    <tr class="text-center">
      <td class="text-left">Desi Ratanasari</td>
      <td class="table-striped-horizontal"><strong class="text-success">100%</strong></td>
      <td><strong class="text-success">100%</strong></td>
      <td class="table-striped-horizontal"><strong class="text-success">100%</strong></td>
      <td><strong class="text-success">100%</strong></td>
      <td class="table-striped-horizontal"><strong class="text-success">100%</strong></td>
      <td><strong class="text-red">0%</strong></td>
      <td class="table-striped-horizontal"><strong class="text-success">100%</strong></td>
      <td><strong class="text-success">100%</strong></td>
      <td class="table-striped-horizontal"><strong class="text-success">100%</strong></td>
      <td><strong class="text-success">100%</strong></td>
      <td class="table-striped-horizontal"><strong class="text-red">0%</strong></td>
      <td><strong class="text-success">100%</strong></td>
      <td class="table-striped-horizontal"><strong class="text-red">0%</strong></td>
      <td><strong class="text-success">100%</strong></td>
      <td class="table-striped-horizontal"><strong class="text-red">0%</strong></td>
      <td><strong class="text-red">0%</strong></td>
      <td class="table-striped-horizontal"><strong class="text-red">0%</strong></td>
      <td class="text-green text-center text-score">15/17</td>
      <td class="text-green text-center text-score">80</td>
    </tr>
    <tr class="text-center">
      <td class="text-left">Galih Pratama</td>
      <td class="table-striped-horizontal"><strong class="text-success">100%</strong></td>
      <td><strong class="text-red">0%</strong></td>
      <td class="table-striped-horizontal"><strong class="text-success">100%</strong></td>
      <td><strong class="text-success">100%</strong></td>
      <td class="table-striped-horizontal"><strong class="text-success">100%</strong></td>
      <td><strong class="text-red">0%</strong></td>
      <td class="table-striped-horizontal"><strong class="text-success">100%</strong></td>
      <td><strong class="text-red">0%</strong></td>
      <td class="table-striped-horizontal"><strong class="text-red">0%</strong></td>
      <td><strong class="text-success">100%</strong></td>
      <td class="table-striped-horizontal"><strong class="text-success">100%</strong></td>
      <td><strong class="text-red">0%</strong></td>
      <td class="table-striped-horizontal"><strong class="text-success">100%</strong></td>
      <td><strong class="text-success">100%</strong></td>
      <td class="table-striped-horizontal"><strong class="text-success">100%</strong></td>
      <td><strong class="text-success">100%</strong></td>
      <td class="table-striped-horizontal"><strong class="text-red">0%</strong></td>
      <td class="text-green text-center text-score">13/17</td>
      <td class="text-green text-center text-score">70</td>
    </tr>
    <tr class="text-center">
      <td class="text-left">Aqmarina Fauhan</td>
      <td class="table-striped-horizontal"><strong class="text-success">100%</strong></td>
      <td><strong class="text-red">0%</strong></td>
      <td class="table-striped-horizontal"><strong class="text-success">100%</strong></td>
      <td><strong class="text-success">100%</strong></td>
      <td class="table-striped-horizontal"><strong class="text-success">100%</strong></td>
      <td><strong class="text-success">100%</strong></td>
      <td class="table-striped-horizontal"><strong class="text-red">0%</strong></td>
      <td><strong class="text-success">100%</strong></td>
      <td class="table-striped-horizontal"><strong class="text-red">0%</strong></td>
      <td><strong class="text-success">100%</strong></td>
      <td class="table-striped-horizontal"><strong class="text-success">100%</strong></td>
      <td><strong class="text-red">0%</strong></td>
      <td class="table-striped-horizontal"><strong class="text-red">0%</strong></td>
      <td><strong class="text-success">100%</strong></td>
      <td class="table-striped-horizontal"><strong class="text-success">100%</strong></td>
      <td><strong class="text-red">0%</strong></td>
      <td class="table-striped-horizontal"><strong class="text-success">100%</strong></td>
      <td class="text-green text-center text-score">14/17</td>
      <td class="text-green text-center text-score">75</td>
    </tr>
    <tr class="table-exam-header text-green">
      <td></td>
      <td class="table-striped-horizontal">100%</td>
      <td>66%</td>
      <td class="table-striped-horizontal">66%</td>
      <td>33%</td>
      <td class="table-striped-horizontal">33%</td>
      <td>66%</td>
      <td class="table-striped-horizontal">66%</td>
      <td>100%</td>
      <td class="table-striped-horizontal">100%</td>
      <td>100%</td>
      <td class="table-striped-horizontal">33%</td>
      <td>0%</td>
      <td class="table-striped-horizontal">66%</td>
      <td>66%</td>
      <td class="table-striped-horizontal">33%</td>
      <td>33%</td>
      <td class="table-striped-horizontal">100%</td>
      <td></td>
      <td></td>
    </tr>
  </table>
</div>
