<div class="col-md-4">
    <div class="col-card row">
      <div class="col-md-12">
        <div class="course-cover-image">
            <img src="https://swanlake1701.files.wordpress.com/2013/12/korean-book-haul.jpg"
                 alt="">
        </div>
        <div class="row">
          <div class="title-top-left">
              <h3>Bahasa Korea</h3>
              <p>
                  <i class="fa fa-building-o"></i> <strong>XII MIA 1</strong> <br>
                  <i class="fa fa-user-circle-o"></i> <strong>Galih Pratama</strong>
              </p>
          </div>
        </div>
      </div>
      <div class="col-md-12">
          <div class="row no-gutter row-accumulate">
              <div class="col-md-4 col-sm-4 col-xs-4 row-accumulate-left">
                  <div class="accumulate-score accumulate-yellow">10</div>
                  <div class="accumulate-description">
                      Materi
                  </div>
              </div>
              <div class="col-md-4 col-sm-4 col-xs-4 row-accumulate-left">
                  <div class="accumulate-score accumulate-red">5</div>
                  <div class="accumulate-description">
                      Tugas
                  </div>
              </div>
              <div class="col-md-4 col-sm-4 col-xs-4 row-accumulate-right">
                  <div class="accumulate-score accumulate-green">2</div>
                  <div class="accumulate-description">
                      Ujian
                  </div>
              </div>
          </div>
      </div>
      <div class="col-md-12">
         <div class="row row-btn-integrated-card no-gutter">
             <br>
             <a href="dashboard.php?page=course-student-detail" type="button" class="btn btn-pn-primary btn-pn-card-integrated btn-lg btn-block" name="button">
                 SELENGKAPNYA
             </a>
         </div>
      </div>
      <div class="clearfix"></div>
    </div>
</div>
