<div class="container-fluid">
  <div class="row">
    <?php include '_breadcrumb_course_detail.php'; ?>
    <div class="col-lg-12">
        <h3>
          Matematika XII-IPA-1, Semester 2
          <div class="pull-right"><a href="#" class="btn btn-pn-primary"><i class="fa fa-book"></i> Rekap Nilai dan Rapor</a></div>
        </h3>
        <div class="row">
          <div class="col-md-3">
            <div class="row">
              <div class="col-md-12">
                <div class="col-card">
                  <ul class="nav nav-pills nav-stacked">
                    <li role="presentation" class="active"><a href="dashboard.php?page=course-detail">Silabus dan Jadwal</a></li>
                    <li role="presentation"><a href="dashboard.php?page=course-detail-bank">Bank Soal</a></li>
                    <li role="presentation"><a href="#">Absensi</a></li>
                  </ul>
                </div>
              </div>
              <div class="col-md-12">
                <div class="col-card row">
                  <div class="title-top-left pull-left">
                      <h4>Siswa</h4>
                  </div>
                  <div class="title-top-right pull-right">
                      <a href="#" class="btn-link-pointer"><i class="fa fa-plus-circle"></i> Tambah Siswa</a>
                  </div>
                  <div class="clearfix"></div>
                  <hr style="margin-top: 0;">
                  <div class="row">
                    <div class="col-xs-4">
                        <a href="#" class="thumbnail-plain">
                             <img src="http://lorempixel.com/g/300/300/people/" class="img-responsive">
                        </a>
                    </div>
                     <div class="col-xs-4">
                        <a href="#" class="thumbnail-plain">
                             <img src="http://lorempixel.com/g/300/300/people/" class="img-responsive">
                        </a>
                    </div>
                    <div class="col-xs-4">
                        <a href="#" class="thumbnail-plain">
                             <img src="http://lorempixel.com/g/300/300/people/" class="img-responsive">
                        </a>
                    </div>
                    <div class="col-xs-4">
                        <a href="#" class="thumbnail-plain">
                             <img src="http://lorempixel.com/g/300/300/people/" class="img-responsive">
                        </a>
                    </div>
                    <div class="col-xs-4">
                        <a href="#" class="thumbnail-plain">
                             <img src="http://lorempixel.com/g/300/300/people/" class="img-responsive">
                        </a>
                    </div>
                    <div class="col-xs-4">
                        <a href="#" class="thumbnail-plain">
                             <img src="http://lorempixel.com/g/300/300/people/" class="img-responsive">
                        </a>
                    </div>
                    <div class="col-xs-4">
                        <a href="#" class="thumbnail-plain">
                             <img src="http://lorempixel.com/g/300/300/people/" class="img-responsive">
                        </a>
                    </div>
                    <div class="col-xs-4">
                        <a href="#" class="thumbnail-plain">
                             <img src="http://lorempixel.com/g/300/300/people/" class="img-responsive">
                        </a>
                    </div>
                    <div class="col-xs-4">
                        <a href="#" class="thumbnail-plain">
                             <img src="http://lorempixel.com/g/300/300/people/" class="img-responsive">
                        </a>
                    </div>
                    <div class="col-xs-4">
                        <a href="#" class="thumbnail-plain">
                             <img src="http://lorempixel.com/g/300/300/people/" class="img-responsive">
                        </a>
                    </div>
                    <div class="col-xs-4">
                        <a href="#" class="thumbnail-plain">
                             <img src="http://lorempixel.com/g/300/300/people/" class="img-responsive">
                        </a>
                    </div>
                    <div class="col-xs-4">
                        <a href="#" class="thumbnail-plain">
                             <img src="http://lorempixel.com/g/300/300/people/" class="img-responsive">
                        </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-9">
            <div class="col-card">
              <h4><strong>Materi Pokok : Eksponen dan Logaritma</strong></h4>
              <hr>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="libraries/canvasjs-1.8.0/canvasjs.min.js"></script>
<script type="text/javascript" src="libraries/jquery/jquery.min.js"></script>
<script type="text/javascript" src="scripts/course_detail.js"></script>
