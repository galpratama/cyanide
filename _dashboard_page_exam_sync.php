<div class="container-fluid">
  <div class="row">
    <?php include '_breadcrumb_exam.php'; ?>
    <div class="col-lg-12">
        <h3>Ujian
          <small class="hidden-xs">Sinkronasi Data Ujian Anda</small>
          <hr>
        </h3>
        <div class="row">
          <div class="col-md-5">
          <h3><i class="fa fa-cloud"></i> Ujian Anda di Online</h3>
          <p>Ujian anda yang tersimpan di cloud server.</p>          
            <div class="col-card col-sync">
              <select name="from" id="multiselect" class="form-control" size="8" multiple="multiple" size="20">
              <optgroup label="Ujian dari Edubox">
                <option value="Matematika">Ujian Matematika</option>
                <option value="Fisika">Ujian Fisika</option>
                <option value="Biologi">Ujian Biologi</option>
                <option value="IPA">Ujian IPA</option>
                <option value="IPS">Ujian IPS</option>
              </optgroup>
              <optgroup label="Ujian dari Zenius">
                <option value="Kewarganegaraan">Ujian Kewarganegaraan</option>
                <option value="Pemrograman Dasar">Ujian Pemrograman Dasar</option>
                <option value="Desain Grafis">Ujian Desain Grafis</option>
                <option value="Fotografi">Ujian Fotografi</option>
                <option value="Manajemen ProyekIPA">Ujian Manajemen Proyek</option>
                <option value="IPS">Ujian IPS</option>
                <option value="Kewarganegaraan">Ujian Kewarganegaraan</option>
              </optgroup>
            </select>
            </div>
          </div>
          <div class="col-md-2">
          <h3>&nbsp;</h3>    
          <p>&nbsp;</p>                
            <div class="col-card">
              <button type="button" id="multiselect_rightAll" class="btn btn-block btn-primary"><i class="fa fa-forward"></i> Semua</button>
              <button type="button" id="multiselect_rightSelected" class="btn btn-block btn-success"><i class="fa fa-chevron-right"></i> Masukkan</button>
              <button type="button" id="multiselect_leftSelected" class="btn btn-block btn-danger"><i class="fa fa-chevron-left"></i> Keluarkan</button>
              <button type="button" id="multiselect_leftAll" class="btn btn-block btn-primary"><i class="fa fa-backward"></i> Semua</button>
            </div>
          </div>
          <div class="col-md-5 col-sync">
            <h3><i class="fa fa-external-link"></i> Ujian Anda di Offline</h3>
            <p>Ujian anda yang akan disimpan di offline dan dapat diujikan kepada siswa</p>         
            <div class="col-card">
              <select name="to" id="multiselect_to" class="form-control" size="8" multiple="multiple">
              </select>
            </div>
          </div>
          <div class="col-md-12">
            <button class="btn btn-block btn-lg btn-success" id="processSync">
              <i class="fa fa-cloud"></i> Sinkronasi data ujian dari cloud
            </button>
          </div>
          <div class="col-md-12">
          <br>
            <div class="progress">
                <div class="progress-bar progress-bar-striped progress-bar-success progress-bar-animated" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
            </div>
            <div class="alert alert-success" role="alert">Selamat! Data Ujian anda berhasil disinkronasi ke data offline!</div>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>

<script type="text/javascript" src="libraries/canvasjs-1.8.0/canvasjs.min.js"></script>
<script type="text/javascript" src="scripts/pie_chart.js"></script>

<style>
.alert {
    display: none;
}
</style>