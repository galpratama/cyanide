<!-- Libraries -->
<link rel="stylesheet" href="libraries/bootstrap/css/bootstrap.min.css" />
<link rel="stylesheet" href="libraries/bootstrap-select/css/bootstrap-select.min.css" />
<link rel="stylesheet" href="libraries/bootstrap-notifications/bootstrap-notifications.min.css" />
<link rel="stylesheet" href="libraries/font-awesome/css/font-awesome.min.css" />
<link rel="stylesheet" href="libraries/rangeslider.js-2.2.1/rangeslider.css" />
<link rel="stylesheet" href="libraries/pplayer/assets/pplayer.css" />

<!-- Stylesheet -->
<link rel="stylesheet" type="text/css" href="css/main.css">
