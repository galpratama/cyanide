<div class="col-md-4">
    <div class="col-card row">
      <div class="col-md-12">
        <div class="row">
          <div class="title-top-left">
              <h3>Ujian Tengah <br>Semester</h3>
              <p>
                  <span class="label label-success">Aktif</span>
              </p>
              <p>
                  Bahasa Indonesia
                  <br>
                  XII - IPA - 1
              </p>
          </div>
          <div class="toggle-top-right">
            <div class="pn-switch">
                <input type="checkbox" name="switch1" class="pn-switch-checkbox" id="switch1">
                <label class="pn-switch-label" for="switch1"></label>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-12">
        <br>
        <div class="chart-pie">
          <div id="chartPieContainer1"
               style="height: 200px; width: 100%;">
          </div>
          <div class="total" id="total-1">300</div>
        </div>
        <br>
      </div>
      <div class="col-md-12">
          <div class="row no-gutter row-accumulate">
              <div class="col-md-6 col-sm-6 col-xs-6 row-accumulate-left">
                  <div class="accumulate-score accumulate-red">27</div>
                  <div class="accumulate-description">
                      Siswa <br>
                      Sudah Selesai
                  </div>
              </div>
              <div class="col-md-6 col-sm-6 col-xs-6 row-accumulate-right">
                  <div class="accumulate-score accumulate-green">17</div>
                  <div class="accumulate-description">
                      Siswa <br>
                      Belum Selesai
                  </div>
              </div>
          </div>
      </div>
      <div class="col-md-12">
         <div class="row row-btn-integrated-card no-gutter">
             <br>
             <a href="dashboard.php?page=exam-detail" type="button" class="btn btn-pn-primary btn-pn-card-integrated btn-lg btn-block" name="button">
                 SELENGKAPNYA
             </a>
         </div>
      </div>
      <div class="clearfix"></div>
    </div>
</div>
