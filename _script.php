<!-- Libraries -->
<script type="text/javascript" src="libraries/jquery/jquery.min.js"></script>
<script type="text/javascript" src="libraries/vue/vue.development.js"></script>
<script type="text/javascript" src="libraries/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="libraries/bootstrap-select/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="libraries/readmore/readmore.min.js"></script>
<script type="text/javascript" src="libraries/rangeslider.js-2.2.1/rangeslider.min.jss"></script>

<script type="text/javascript" src="libraries/typeahead/bootstrap3-typeahead.js"></script>
<script type="text/javascript">
  // Documentation : https://github.com/bassjobsen/Bootstrap-3-Typeahead
  $.get('example_collection.json', function(data){
    $("#name-typeahead").typeahead(
      {
        source:data
      });
  },'json');
</script>

<script type="text/javascript" src="libraries/tinymce/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="libraries/tinymce/js/tinymce/jquery.tinymce.min.js"></script>
<script>
tinymce.init({
selector: '.rich-textarea',
height: 200,
theme: 'modern',
plugins: [
  'advlist autolink lists link image charmap print preview hr anchor pagebreak',
  'searchreplace wordcount visualblocks visualchars code fullscreen',
  'insertdatetime media nonbreaking save table contextmenu directionality',
  'emoticons template paste textcolor colorpicker textpattern imagetools'
],
toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
toolbar2: 'print preview media | forecolor backcolor emoticons',
image_advtab: true,
});
</script>
<script>
tinymce.init({
selector: '.simple-textarea',
height: 100,
theme: 'modern',
plugins: [
    'advlist autolink lists link image charmap print preview anchor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table contextmenu paste code'
  ],
  menubar: false,
  toolbar: 'styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image'
});
</script>
<script type="text/javascript">
tinymce.init({
  selector: '.jawaban',
  inline: true,
  // plugins: [
  //   'advlist autolink lists link image charmap print preview hr anchor pagebreak',
  //   'searchreplace wordcount visualblocks visualchars code fullscreen',
  //   'insertdatetime media nonbreaking save table contextmenu directionality',
  //   'emoticons template paste textcolor colorpicker textpattern imagetools'
  // ],
  // toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
  // toolbar2: 'print preview media | forecolor backcolor emoticons',
  setup : function(editor) {
    editor.on('init', function () {
        // Add class on init
        // this also sets the empty class on the editor on init
        tinymce.DOM.addClass( editor.bodyElement, 'jawaban empty' );
    });
    // You CAN do it on 'change' event, but tinyMCE sets debouncing on that event
    // so for a tiny moment you would see the placeholder text and the text you you typed in the editor
    // the selectionchange event happens a lot more and with no debouncing, so in some situations
    // you might have to go back to the change event instead.
     editor.on('selectionchange', function () {
         if ( editor.getContent() === "" ) {
             tinymce.DOM.addClass( editor.bodyElement, 'empty' );
         } else {
             tinymce.DOM.removeClass( editor.bodyElement, 'empty' );
         }
     });
  }
});
</script>

<script src="libraries/multiselect/dist/js/multiselect.min.js"></script>
<script>
  $('#multiselect').multiselect({
    afterMoveToRight: function(e){
      console.log(e);
    },
    search: {
        left: '<input type="text" name="q" class="form-control" style="margin-bottom: 5px" placeholder="Cari Ujian..." />',
        right: '<input type="text" name="q" class="form-control"  style="margin-bottom: 5px"  placeholder="Cari Ujian..." />',
    },
    fireSearch: function(value) {
        return value.length > 3;
    }
  });

  $('#processSync').click(function (){
    $('#processSync').prop('disabled', true); 
    var $progress = $('.progress');
    var $progressBar = $('.progress-bar');
    var $alert = $('.alert');

    setTimeout(function() {
        $progressBar.css('width', '10%');
        setTimeout(function() {
            $progressBar.css('width', '30%');
            setTimeout(function() {
                $progressBar.css('width', '100%');
                setTimeout(function() {
                    $progress.css('display', 'none');
                    $alert.css('display', 'block');
                }, 500); // WAIT 5 milliseconds
            }, 2000); // WAIT 2 seconds
        }, 1000); // WAIT 1 seconds
    }, 1000); // WAIT 1 second
  })
  
</script>

<!-- App script -->
<script type="text/javascript" src="scripts/main.js"></script>
