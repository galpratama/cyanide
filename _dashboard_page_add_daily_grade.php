<div class="container-fluid">
  <div class="row">
    <?php include '_breadcrumb_grade_add_daily.php'; ?>
    <div class="col-lg-12">
      <h3>Membuat Penilaian Harian</small></h3>
      <div class="row">
        <div class="col-md-12">
          <div class="col-card">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="namaUjian">Nama Penilaian</label>
                  <input type="text" class="form-control input-pn input-lg" id="namaPenilaian" placeholder="Isi nama penilaian disini (contoh: Penilaian Bahasa Indonesia) …">
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label for="namaPelajaran">Pilih Nama Mata Pelajaran dan Kelas</label>
                  <select name="namaPelajaran" class="selectpicker form-control" data-style="btn-default input-lg" data-live-search="true">
                    <option>Matematika XII-IPA-1</option>
                  </select>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label for="materiPokok">Pilih Materi Pokok dan Kompetensi</label>
                  <select name="materiPokok" class="selectpicker form-control" data-style="btn-default input-lg" data-live-search="true">
                    <option>Integral Lipat - KD 3.1 Siswa Memahami Integral</option>
                  </select>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label for="jenisPenilaian">Pilih Jenis Penilaian</label>
                  <select name="jenisPenilaian" class="selectpicker form-control" data-style="btn-default input-lg" data-live-search="false">
                    <option>Portfolio</option>
                  </select>
                </div>
              </div>
            </div>
            <br>
            <div class="row">
              <div class="col-md-12">
                <button type="button" class="btn btn-pn-primary btn-lg btn-pn-round btn-block next-step">Beri Nilai Siswa</button>
              </div>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>

<script type="text/javascript" src="libraries/canvasjs-1.8.0/canvasjs.min.js"></script>
<script type="text/javascript" src="scripts/pie_chart.js"></script>
