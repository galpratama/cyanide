<div class="container-fluid">
  <div class="row">
    <?php include '_breadcrumb_add_soal.php'; ?>
    <div class="col-lg-12">
      <h2>
        Pratinjau Soal Baru
        <div class="pull-right">
          <button type="button" class="btn btn-warning btn-pn-round next-step"><i class="fa fa-chevron-left"></i> Kembali</button>
          <button type="button" class="btn btn-pn-primary btn-pn-round next-step"><i class="fa fa-save"></i> Simpan Semua Soal</button>
        </div>
      </h2>
      <div class="row">
        <div class="col-md-3">
          <h4>
            Daftar Soal
          </h4>
          <div class="col-card">
            <div class="list-soal-container">
              <ul class="nav nav-pills">
                <div class="hidden-md hidden-lg">
                  &nbsp;&nbsp;<i class="fa fa-arrow-up fa-2x"></i>
                </div>
                <li class="list-soal active"><a href="#step1" id="coba1" data-local="768" class="btn btn-soal" data-toggle="tab" data-step="1">1</a></li>
                <li class="list-soal"><a href="#step2" id="coba2" data-local="769" class="btn btn-soal" data-toggle="tab" data-step="2" >2</a></li>
                <li class="list-soal"><a href="#step2" id="coba2" data-local="769" class="btn btn-soal" data-toggle="tab" data-step="2" >3</a></li>
                <li class="list-soal"><a href="#step2" id="coba2" data-local="769" class="btn btn-soal" data-toggle="tab" data-step="2" >4</a></li>
                <li class="list-soal"><a href="#step2" id="coba2" data-local="769" class="btn btn-soal" data-toggle="tab" data-step="2" >5</a></li>
                <li class="list-soal"><a href="#step2" id="coba2" data-local="769" class="btn btn-soal" data-toggle="tab" data-step="2" >6</a></li>
                <li class="list-soal"><a href="#step2" id="coba2" data-local="769" class="btn btn-soal" data-toggle="tab" data-step="2" >7</a></li>
                <li class="list-soal"><a href="#step2" id="coba2" data-local="769" class="btn btn-soal" data-toggle="tab" data-step="2" >8</a></li>
                <li class="list-soal"><a href="#step2" id="coba2" data-local="769" class="btn btn-soal" data-toggle="tab" data-step="2" >9</a></li>
                <li class="list-soal"><a href="#step2" id="coba2" data-local="769" class="btn btn-soal" data-toggle="tab" data-step="2" >10</a></li>
                <li class="list-soal"><a href="#step2" id="coba2" data-local="769" class="btn btn-soal" data-toggle="tab" data-step="2" >7</a></li>
                <li class="list-soal"><a href="#step2" id="coba2" data-local="769" class="btn btn-soal" data-toggle="tab" data-step="2" >8</a></li>
                <li class="list-soal"><a href="#step2" id="coba2" data-local="769" class="btn btn-soal" data-toggle="tab" data-step="2" >9</a></li>
                <li class="list-soal"><a href="#step2" id="coba2" data-local="769" class="btn btn-soal" data-toggle="tab" data-step="2" >10</a></li>
                <div class="hidden-md hidden-lg">
                  &nbsp;&nbsp;<i class="fa fa-arrow-down fa-2x"></i>
                </div>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-md-9">
          <h4>Pratinjau Soal</h4>
          <div class="col-card">
            <div class="row">
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-12">
                    <div class="rich-textarea-container">
                      <h3>Soal</h3>
                      <hr>
                      <div>
                        <blockquote>
                          "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit rerum voluptas tempore officiis! Iste illo impedit, deserunt, a vero at laborum deleniti explicabo, harum veritatis quas temporibus molestiae sit dicta.?"
                        </blockquote>
                        <div class="pull-right">
                          <a class="btn btn-primary btn-sm" data-toggle="collapse" data-target="#collapseImportSoalWord10" >
                            <i class="fa fa-pencil"></i> Sunting Soal
                          </a>
                        </div>
                        <div class="clearfix"></div>
                        <div class="rich-textarea-container collapse" id="collapseImportSoalWord10">
                          <textarea name="" id="" cols="30" rows="10" class="rich-textarea"></textarea>
                          <br>
                          <button type="button" class="btn btn-primary btn-block" data-toggle="collapse" data-target="#collapseImportSoalWord10"><i class="fa fa-save"></i> Perbarui</button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="rich-textarea-container">
                      <h3>Jawaban</h3>
                      <hr>
                      <div class="exam-answer">
                        <div class="alert alert-success" role="alert">
                          <p class="pull-left">
                            A. Jawaban Paling Benar Sealam Semesta Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam rerum quis assumenda praesentium id amet doloribus quidem.
                          </p>
                          <div class="pull-right">
                            <a class="btn btn-primary btn-sm" data-toggle="collapse" data-target="#collapseImportSoalWord" >
                              <i class="fa fa-pencil"></i>
                            </a>
                          </div>
                          <div class="clearfix"></div>
                          <div class="rich-textarea-container collapse" id="collapseImportSoalWord">
                            <textarea name="" id="" cols="30" rows="10" class="rich-textarea"></textarea>
                            <br>
                            <button type="button" class="btn btn-primary btn-block" data-toggle="collapse" data-target="#collapseImportSoalWord"><i class="fa fa-save"></i> Perbarui</button>
                          </div>
                          <div class="clearfix"></div>
                        </div>
                        <div class="alert alert-danger" role="alert">
                          <p class="pull-left">
                            B. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias, architecto atque aut consequatur, cum in obcaecati odit officia rerum, similique sint.
                          </p>
                          <div class="pull-right">
                            <a class="btn btn-primary btn-sm" data-toggle="collapse" data-target="#collapseImportSoalWord2" >
                              <i class="fa fa-pencil"></i>
                            </a>
                          </div>
                          <div class="clearfix"></div>
                          <div class="rich-textarea-container collapse" id="collapseImportSoalWord2">
                            <textarea name="" id="" cols="30" rows="10" class="rich-textarea"></textarea>
                            <br>
                            <button type="button" class="btn btn-primary btn-block" data-toggle="collapse" data-target="#collapseImportSoalWord2"><i class="fa fa-save"></i> Perbarui</button>
                          </div>
                          <div class="clearfix"></div>
                        </div>
                        <div class="alert alert-danger" role="alert">
                          <p class="pull-left">
                            C. L Cem ipsum dolor sit amet, consectetur adipisicing elit. Est nihil praesentium quas quia quod! Neque.
                          </p>
                          <div class="pull-right">
                            <a class="btn btn-primary btn-sm" data-toggle="collapse" data-target="#collapseImportSoalWord3" >
                              <i class="fa fa-pencil"></i>
                            </a>
                          </div>
                          <div class="clearfix"></div>
                          <div class="rich-textarea-container collapse" id="collapseImportSoalWord3">
                            <textarea name="" id="" cols="30" rows="10" class="rich-textarea"></textarea>
                            <br>
                            <button type="button" class="btn btn-primary btn-block" data-toggle="collapse" data-target="#collapseImportSoalWord3"><i class="fa fa-save"></i> Perbarui</button>
                          </div>
                          <div class="clearfix"></div>
                        </div>
                        <div class="alert alert-danger" role="alert">
                          <p class="pull-left">
                            D. Lorem ipsum dolor sit amet, consectetur.
                          </p>
                          <div class="pull-right">
                            <a class="btn btn-primary btn-sm" data-toggle="collapse" data-target="#collapseImportSoalWord4" >
                              <i class="fa fa-pencil"></i>
                            </a>
                          </div>
                          <div class="clearfix"></div>
                          <div class="rich-textarea-container collapse" id="collapseImportSoalWord4">
                            <textarea name="" id="" cols="30" rows="10" class="rich-textarea"></textarea>
                            <br>
                            <button type="button" class="btn btn-primary btn-block" data-toggle="collapse" data-target="#collapseImportSoalWord4"><i class="fa fa-save"></i> Perbarui</button>
                          </div>
                          <div class="clearfix"></div>
                        </div>
                      </div>
                    </div>
                    <div class="row col-button">
                      <div class="col-md-6">
                        <a href="#" class="btn btn-info btn-sm btn-block">
                          <i class="fa fa-chevron-left"></i> Soal Sebelumnya
                        </a>
                        <br>
                      </div>
                      <div class="col-md-6">
                        <a href="#" class="btn btn-info btn-sm btn-block">
                          Soal Selanjutnya <i class="fa fa-chevron-right"></i>
                        </a>
                      </div>
                    </div><!-- /.col-button -->
                  </div>
                </div>
              </div>
            </div>
          </div>
         </div>
        <div class="col-md-9 col-md-offset-3">
          <div class="">
            <div class="row">
              <div class="col-md-6">
                <button type="button" class="btn btn-warning btn-lg btn-pn-round btn-block next-step"><i class="fa fa-chevron-left"></i> Kembali</button>
              </div>
              <div class="col-md-6">
                <button type="button" class="btn btn-pn-primary btn-lg btn-pn-round btn-block next-step"><i class="fa fa-save"></i> Simpan Semua Soal</button>
              </div>
            </div>
          </div>
        </div>
        </div><!-- /.col-md-8 -->
        </div><!-- /.row -->
      </div><!-- /.row -->
    </div><!-- /.col-md-12 -->
  </div><!-- /.row -->
</div><!-- /.container-fluid -->
<script type="text/javascript" src="libraries/canvasjs-1.8.0/canvasjs.min.js"></script>
<script type="text/javascript" src="scripts/column_chart.js"></script>
<script type="text/javascript" src="libraries/jquery/jquery.min.js"></script>
<script>
  if ($(window).width() > 960) {
    $("#wrapper").toggleClass("toggled");
  }
</script>
