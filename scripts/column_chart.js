window.onload = function () {
  
  
var chart = new CanvasJS.Chart("chartContainer",
  {
    animationEnabled: true,
    height: 323,
    axisY:{
      gridThickness: 1,
      lineThickness: 0,
      tickThickness: 0,
      labelFontFamily: 'Proxima Nova'
    },
    axisX:{
      gridThickness: 0,
      lineThickness: 0,
      tickThickness: 0,
      labelFontFamily: 'Proxima Nova'
    },
    data: [
    {
      type: "column", //change it to line, area, bar, pie, etc
      dataPoints: [
        { x: 1, y: 0, label: "0-10" },
        { x: 2, y: 0, label: "11-20" },
        { x: 3, y: 30, label: "21-30" },
        { x: 4, y: 17, label: "31-40" },
        { x: 5, y: 84, label: "41-50" },
        { x: 6, y: 88, label: "51-60" },
        { x: 7, y: 70, label: "61-70"},
        { x: 8, y: 69, label: "71-80" },
        { x: 9, y: 39, label: "81-90" },
        { x: 10, y: 19, label: "91-100" },
      ]
    }
    ]
  });

 chart.render();

}
