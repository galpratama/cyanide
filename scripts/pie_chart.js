// Chart Color for Pie Chart
CanvasJS
  .addColorSet(
    "pinisiPercentChart", [
      "#51C553", // green
      "#D10025", // red
  ]);

var pieChart1 = new CanvasJS.Chart("chartPieContainer1",
{
  colorSet: "pinisiPercentChart",
    axisY:{
       valueFormatString: " ",
       tickLength: 0
    },
    axisX:{
        valueFormatString: " ",
        tickLength: 0
    },
    interactivityEnabled: false,
    data: [
    {
      type: "doughnut",
      startAngle: 270,
      dataPoints: [
      { x: 10, y: 70 },
      { x: 20, y: 30}
      ]
    }
    ]
  });


var pieChart2 = new CanvasJS.Chart("chartPieContainer2",
{
  colorSet: "pinisiPercentChart",
    axisY:{
       valueFormatString: " ",
       tickLength: 0
    },
    axisX:{
        valueFormatString: " ",
        tickLength: 0
    },
    interactivityEnabled: false,
    data: [
    {
      type: "doughnut",
      startAngle: 270,
      dataPoints: [
      { x: 10, y: 40 },
      { x: 20, y: 60}
      ]
    }
    ]
  });


var pieChart3 = new CanvasJS.Chart("chartPieContainer3",
{
  colorSet: "pinisiPercentChart",
    axisY:{
       valueFormatString: " ",
       tickLength: 0
    },
    axisX:{
        valueFormatString: " ",
        tickLength: 0
    },
    interactivityEnabled: false,
    data: [
    {
      type: "doughnut",
      startAngle: 270,
      dataPoints: [
      { x: 10, y: 50 },
      { x: 20, y: 50}
      ]
    }
    ]
  });

pieChart1.render();
pieChart2.render();
pieChart3.render();

var dps1 = pieChart1.options.data[0].dataPoints;
var dps2 = pieChart2.options.data[0].dataPoints;
var dps3 = pieChart3.options.data[0].dataPoints;

document.getElementById("total-1").innerHTML = dps1[0].y + '%';
document.getElementById("total-2").innerHTML = dps2[0].y + '%';
document.getElementById("total-3").innerHTML = dps3[0].y + '%';
