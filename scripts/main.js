$(document).ready(function () {
  $('.collapsible-row').readmore({
    speed: 500,
    collapsedHeight: 20,
    moreLink: '<a class="clickable-more-row" href="#">Read more</a>',
    lessLink: '<div><a class="btn btn-danger btn-sm" style="margin-top: 10px;" href="#"><i class="fa fa-times-circle"></i> Tutup Soal</a></div>'
  });
  $("[data-toggle=popover]").popover({
    html : true,
    content: function() {
      var content = $(this).attr("data-popover-content");
      return $(content).children(".popover-body").html();
    }
  });
  // Initialize a new plugin instance for all
  // e.g. $('input[type="range"]') elements.
  $('input[type="range"]').rangeslider();

  // Destroy all plugin instances created from the
  // e.g. $('input[type="range"]') elements.
  $('input[type="range"]').rangeslider('destroy');

  // Update all rangeslider instances for all
  // e.g. $('input[type="range"]') elements.
  // Usefull if you changed some attributes e.g. `min` or `max` etc.
  $('input[type="range"]').rangeslider('update', true);

  $('input[type="range"]').rangeslider({

    // Feature detection the default is `true`.
    // Set this to `false` if you want to use
    // the polyfill also in Browsers which support
    // the native <input type="range"> element.
    polyfill: true,

    // Default CSS classes
    rangeClass: 'rangeslider',
    disabledClass: 'rangeslider--disabled',
    horizontalClass: 'rangeslider--horizontal',
    verticalClass: 'rangeslider--vertical',
    fillClass: 'rangeslider__fill',
    handleClass: 'rangeslider__handle',

    // Callback function
    onInit: function() {},

    // Callback function
    onSlide: function(position, value) {},

    // Callback function
    onSlideEnd: function(position, value) {}
  });
});