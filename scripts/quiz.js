if ($(window).width() > 960) {
  $("#wrapper").toggleClass("toggled");
}

  // 15 days from now!
  function get15dayFromNow() {
    return new Date(new Date().valueOf() + 24 * 60 * 60 * 1000);
  }

  var $clock = $('#clock');

  $clock.countdown(get15dayFromNow(), function(event) {
    $(this).html(event.strftime('%H:%M:%S'));
  });


// Fabric js js related
(function() {
  // shorthand for getelementbyid
  var elementID = function(id){return document.getElementById(id)};

  // initialize canvas
  var canvas = this.__canvas = new fabric.Canvas('c', {
    isDrawingMode: false,
    // selection : false,
    // controlsAboveOverlay:true,
    // centeredScaling:true,
    // allowTouchScrolling: true
  });

  // set canvas background color
  canvas.setBackgroundColor('rgba(0, 0, 0, 0.02)', canvas.renderAll.bind(canvas));

  // Upload image functionality
  document.getElementById('imgLoader').onchange = function handleImage(e) {
    var reader = new FileReader();
    reader.onload = function (event) { console.log('Uploading');
      var imgObj = new Image();
      imgObj.src = event.target.result;
      imgObj.onload = function () {
        var image = new fabric.Image(imgObj);
        image.set({
          left: 0,
          top: 0,
          angle: 0,
          padding: 10,
          cornersize: 10
        });
        canvas.add(image);
      }

    }
    reader.readAsDataURL(e.target.files[0]);
  }

  // copy paste functionality fabric js


  createListenersKeyboard();

  function createListenersKeyboard() {
    document.onkeydown = onKeyDownHandler;
    //document.onkeyup = onKeyUpHandler;
  }

  function onKeyDownHandler(event) {
    //event.preventDefault();

    var key;
    if(window.event){
      key = window.event.keyCode;
    }
    else{
      key = event.keyCode;
    }

    switch(key){
        //////////////
        // Shortcuts
        //////////////
        // Copy (Ctrl+C)
      case 67: // Ctrl+C
        if(ableToShortcut()){
          if(event.ctrlKey){
            event.preventDefault();
            copy();
          }
        }
        break;
        // Paste (Ctrl+V)
      case 86: // Ctrl+V
        if(ableToShortcut()){
          if(event.ctrlKey){
            event.preventDefault();
            paste();
          }
        }
        break;
      case 8: // Ctrl+V
        if(ableToShortcut()){
          deleteObject();
        }
        break;
      default:
        // TODO
        break;
    }
  }


  function ableToShortcut(){
    /*
     TODO check all cases for this

     if($("textarea").is(":focus")){
     return false;
     }
     if($(":text").is(":focus")){
     return false;
     }
     */
    return true;
  }

  function deleteObject() {
    var activeObject = canvas.getActiveObject(),
        activeGroup = canvas.getActiveGroup();
    if (activeObject) {
      if (confirm('Apakah kamu yakin?')) {
        canvas.remove(activeObject);
      }
    }
    else if (activeGroup) {
      if (confirm('Apakah kamu yakin?')) {
        var objectsInGroup = activeGroup.getObjects();
        canvas.discardActiveGroup();
        objectsInGroup.forEach(function(object) {
          canvas.remove(object);
        });
      }
    }
  }

  function copy(){
    if(canvas.getActiveGroup()){
      for(var i in canvas.getActiveGroup().objects){
        var object = fabric.util.object.clone(canvas.getActiveGroup().objects[i]);
        object.set("top", object.top+5);
        object.set("left", object.left+5);
        copiedObjects[i] = object;
      }
    }
    else if(canvas.getActiveObject()){
      var object = fabric.util.object.clone(canvas.getActiveObject());
      object.set("top", object.top+5);
      object.set("left", object.left+5);
      copiedObject = object;
      copiedObjects = new Array();
    }
  }

  function paste(){
    if(copiedObjects.length > 0){
      for(var i in copiedObjects){
        canvas.add(copiedObjects[i]);
      }
    }
    else if(copiedObject){
      canvas.add(copiedObject);
    }
    canvas.renderAll();
  }

  canvas.on('object:added',function(){
    if(!isRedoing){
      h = [];
    }
    isRedoing = false;
  });

  var isRedoing = false;
  var h = [];
  function undo(){
    if(canvas._objects.length>0){
      h.push(canvas._objects.pop());
      canvas.renderAll();
    }
  }
  function redo(){

    if(h.length>0){
      isRedoing = true;
      canvas.add(h.pop());
    }
  }

  // fabric js draw image code
  fabric.Object.prototype.transparentCorners = false;

  // initialize element to variables
  var drawingModeEl = elementID('drawing-mode'),
      selectionModeEl = elementID('selection-mode'),
      drawingOptionsEl = elementID('drawing-mode-options'),
      drawingColorEl = elementID('drawing-color'),
      drawingLineWidthEl = elementID('drawing-line-width'),
      deleteEl = elementID('delete-element'),
      sendBackwardEl = elementID('send-backward'),
      bringForwardEl = elementID('bring-forward'),
      fillToolboxEl = elementID('fill-toolbox'),
      fontToolboxEl = elementID('font-toolbox'),
      addTextEl = elementID('add-text'),
      clearEl = elementID('clear-canvas');

  clearEl.onclick = function() {
    if (confirm('Apakah kamu yakin untuk menghapus semuanya?')) {
      canvas.clear();
    }
  };

  $('#addCircle').click(function(){
    canvas.add(new fabric.Circle({
      radius: 70, fill: 'gray', left: 100, top: 100,
    }));
  });

  $('#addRect').click(function(){
    canvas.add(new fabric.Rect({
      top: 100, left: 100, width: 100, height: 100, fill: 'gray'
    }));
  });

  $('#addTriangle').click(function(){
    canvas.add(new fabric.Triangle({
      top: 100, left: 100, width: 100, height: 100, fill: 'gray'
    }));
  });

  $('#addGrid').click(function(){
    var url = window.location.href;
    url = url.substring(0, url.lastIndexOf("/") + 1);
    fabric.Image.fromURL(url + 'images/grid-canvas.jpg', function(img) {
      canvas.add(img.set({ left: 0, top: 0, angle: 0 }).scale(1));
    });
  });


  $('#undo-action').click(function(){
    undo();
  }) ;

  $('#redo-action').click(function(){
    redo();
  }) ;

  $('#zoomIn').click(function(){
    canvas.setZoom(canvas.getZoom() * 1.1 ) ;
  }) ;

  $('#zoomOut').click(function(){
    canvas.setZoom(canvas.getZoom() / 1.1 ) ;
  }) ;

  $('#goRight').click(function(){
    var units = 10 ;
    var delta = new fabric.Point(units,0) ;
    canvas.relativePan(delta) ;
  }) ;

  $('#goLeft').click(function(){
    var units = 10 ;
    var delta = new fabric.Point(-units,0) ;
    canvas.relativePan(delta) ;
  }) ;
  $('#goUp').click(function(){
    var units = 10 ;
    var delta = new fabric.Point(0,-units) ;
    canvas.relativePan(delta) ;
  }) ;

  $('#goDown').click(function(){
    var units = 10 ;
    var delta = new fabric.Point(0,units) ;
    canvas.relativePan(delta) ;
  }) ;

  $("#canvas2png").click(function(){
    canvas.isDrawingMode = false;

    if(!window.localStorage){alert("This function is not supported by your browser."); return;}
    // to PNG
    $( "#image-preview" ).html(
        "<img style='width: 100%' class='thumbnail' src='" + canvas.toDataURL("image/jpeg", 0.8) + "'/>" +
        "<label for=''>Base64 Code (DEVELOPERS ONLY)</label>" +
        "<pre class='well'>"+ canvas.toDataURL("image/jpeg", 0.8) +"</pre>" );
  });

  fillToolboxEl.onchange = function() {
    var obj = canvas.getActiveObject();
    if(obj){
      obj.setFill($(this).val());
    }
    canvas.renderAll();
  };

  fontToolboxEl.onchange = function () {
    var obj = canvas.getActiveObject();
    if(obj){
      obj.setFontFamily($(this).val());
    }
    canvas.renderAll();
  };

  addTextEl.onclick = function() {
    var oText = new fabric.IText('Klik disini untuk mengganti teks', {
      left: 100,
      top: 100 ,
    });

    canvas.add(oText);
    canvas.setActiveObject(oText);
    $('#fill-toolbox, #fill-toolbox').trigger('change');
    oText.bringToFront();
  };

  bringForwardEl.onclick = function() {
    var activeObject=canvas.getActiveObject(),
        activeGroup=canvas.getActiveGroup();
    if (activeObject) {
      activeObject.bringForward();
      canvas.renderAll();
    }
    else if (activeGroup) {
      canvas.getActiveGroup().forEachObject(function(o){canvas.bringForward(o); });
      //activeGroup.bringForward();
      canvas.renderAll();
    }
  };

  sendBackwardEl.onclick = function() {
    var activeObject=canvas.getActiveObject(),
        activeGroup=canvas.getActiveGroup();
    if (activeObject) {
      activeObject.sendBackwards();
      canvas.renderAll();
    }
    else if (activeGroup) {
      canvas.getActiveGroup().forEachObject(function(o){canvas.sendBackwards(o); });
      //activeGroup.sendBackwards();
      canvas.renderAll();
    }
  };

  deleteEl.onclick = function () {
    deleteObject();
  }

  drawingModeEl.onclick = function() {
    canvas.isDrawingMode = true;
    drawingOptionsEl.style.display = '';
  };

  selectionModeEl.onclick = function() {
    canvas.isDrawingMode = false;
    drawingOptionsEl.style.display = 'none';
  };

  if (fabric.PatternBrush) {
    var vLinePatternBrush = new fabric.PatternBrush(canvas);
    vLinePatternBrush.getPatternSrc = function() {

      var patternCanvas = fabric.document.createElement('canvas');
      patternCanvas.width = patternCanvas.height = 10;
      var ctx = patternCanvas.getContext('2d');

      ctx.strokeStyle = this.color;
      ctx.lineWidth = 5;
      ctx.beginPath();
      ctx.moveTo(0, 5);
      ctx.lineTo(10, 5);
      ctx.closePath();
      ctx.stroke();

      return patternCanvas;
    };

    var hLinePatternBrush = new fabric.PatternBrush(canvas);
    hLinePatternBrush.getPatternSrc = function() {

      var patternCanvas = fabric.document.createElement('canvas');
      patternCanvas.width = patternCanvas.height = 10;
      var ctx = patternCanvas.getContext('2d');

      ctx.strokeStyle = this.color;
      ctx.lineWidth = 5;
      ctx.beginPath();
      ctx.moveTo(5, 0);
      ctx.lineTo(5, 10);
      ctx.closePath();
      ctx.stroke();

      return patternCanvas;
    };

    var squarePatternBrush = new fabric.PatternBrush(canvas);
    squarePatternBrush.getPatternSrc = function() {

      var squareWidth = 10, squareDistance = 2;

      var patternCanvas = fabric.document.createElement('canvas');
      patternCanvas.width = patternCanvas.height = squareWidth + squareDistance;
      var ctx = patternCanvas.getContext('2d');

      ctx.fillStyle = this.color;
      ctx.fillRect(0, 0, squareWidth, squareWidth);

      return patternCanvas;
    };

    var diamondPatternBrush = new fabric.PatternBrush(canvas);
    diamondPatternBrush.getPatternSrc = function() {

      var squareWidth = 10, squareDistance = 5;
      var patternCanvas = fabric.document.createElement('canvas');
      var rect = new fabric.Rect({
        width: squareWidth,
        height: squareWidth,
        angle: 45,
        fill: this.color
      });

      var canvasWidth = rect.getBoundingRectWidth();

      patternCanvas.width = patternCanvas.height = canvasWidth + squareDistance;
      rect.set({ left: canvasWidth / 2, top: canvasWidth / 2 });

      var ctx = patternCanvas.getContext('2d');
      rect.render(ctx);

      return patternCanvas;
    };

    var img = new Image();
    img.src = '../assets/honey_im_subtle.png';

    var texturePatternBrush = new fabric.PatternBrush(canvas);
    texturePatternBrush.source = img;
  }

  elementID('drawing-mode-selector').onchange = function() {

    if (this.value === 'hline') {
      canvas.freeDrawingBrush = vLinePatternBrush;
    }
    else if (this.value === 'vline') {
      canvas.freeDrawingBrush = hLinePatternBrush;
    }
    else if (this.value === 'square') {
      canvas.freeDrawingBrush = squarePatternBrush;
    }
    else if (this.value === 'diamond') {
      canvas.freeDrawingBrush = diamondPatternBrush;
    }
    else if (this.value === 'texture') {
      canvas.freeDrawingBrush = texturePatternBrush;
    }
    else {
      canvas.freeDrawingBrush = new fabric[this.value + 'Brush'](canvas);
    }

    if (canvas.freeDrawingBrush) {
      canvas.freeDrawingBrush.color = drawingColorEl.value;
      canvas.freeDrawingBrush.width = parseInt(drawingLineWidthEl.value, 10) || 1;
    }
  };

  drawingColorEl.onchange = function() {
    canvas.freeDrawingBrush.color = this.value;
  };
  drawingLineWidthEl.onchange = function() {
    canvas.freeDrawingBrush.width = parseInt(this.value, 10) || 1;
    this.previousSibling.innerHTML = this.value;
  };

  if (canvas.freeDrawingBrush) {
    canvas.freeDrawingBrush.color = drawingColorEl.value;
    canvas.freeDrawingBrush.width = parseInt(drawingLineWidthEl.value, 10) || 1;
  }
})();

// Prevent reload, refresh, page changes
window.addEventListener("beforeunload", function (e) {
  var confirmationMessage = 'Apakah kamu yakin?';

  (e || window.event).returnValue = confirmationMessage; //Gecko + IE
  return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
});

if ($(window).width() > 960) {
  $("#wrapper").toggleClass("toggled");
}
