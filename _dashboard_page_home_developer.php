<div class="container-fluid">
  <div class="row">
    <div class="col-lg-12">
      <h2>Selamat Datang, Developer!</h2>
      <h4>Matriks Nilai</h4>
      <div class="col-card">
        <div class="row">
          <div class="chart-pie">
            <div id="chartContainer"
                 style="height: 323px; width: 100%;">
            </div>
            <br />
            <div class="text-center">Matriks Nilai</div><!-- /.text-center -->
          </div>
        </div>
      </div><!-- /.col-card -->
    </div><!-- /.col-lg-12 -->
  </div><!-- /.row -->
</div>

<script type="text/javascript" src="libraries/canvasjs-1.8.0/canvasjs.min.js"></script>
<script type="text/javascript" src="scripts/column_chart.js"></script>
