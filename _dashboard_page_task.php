<div class="container-fluid">
  <div class="row">
    <?php include '_breadcrumb_task.php'; ?>
    <div class="col-lg-12">
        <h3>Tugas Terkini
          <small class="hidden-xs">Daftar Tugas Terbaru Anda</small>
          <div class="pull-right">
            <a href="dashboard.php?page=task-add" class="btn btn-sm btn-pn-primary btn-pn-round">
              <span class="hidden-sm hidden-xs"><i class="fa fa-plus-circle"></i> TAMBAH TUGAS</span>
              <span class="hidden-md hidden-lg"><i class="fa fa-plus-circle"></i></span>
            </a>
          </div>
        </h3>
        <div class="row">
          <?php include '_card_task.php'; ?>
          <?php include '_card_blank_add_task.php'; ?>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="libraries/canvasjs-1.8.0/canvasjs.min.js"></script>
<script>

CanvasJS
  .addColorSet(
    "pinisiPercentChart", [
      "#51C553", // green
      "#D10025", // red
  ]);

var pieChart2 = new CanvasJS.Chart("chartPieContainer2",
{
  colorSet: "pinisiPercentChart",
    axisY:{
       valueFormatString: " ",
       tickLength: 0
    },
    axisX:{
        valueFormatString: " ",
        tickLength: 0
    },
    interactivityEnabled: false,
    data: [
    {
      type: "doughnut",
      startAngle: 270,
      dataPoints: [
      { x: 10, y: 40 },
      { x: 20, y: 60}
      ]
    }
    ]
  });
  pieChart2.render();
  var dps2 = pieChart2.options.data[0].dataPoints;
  document.getElementById("total-2").innerHTML = dps2[0].y + '%';

</script>
