<nav class="navbar navbar-default navbar-pn navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <div class="navbar-brand">
          <div class="brand-primary">
            <button class="btn btn-link btn-md hidden-md hidden-lg" id="menu-toggle"><i class="fa fa-bars"></i></button>
            Edubox 2.0
          </div>
          <div class="brand-secondary hidden-xs">SEKOLAH PINISI</div>
      </div>
    </div>
      <div class="nav navbar-nav navbar-search">
        <form action="#" class="navbar-form-search hidden-xs">
          <span class="fa fa-search navbar-form-search-icon"></span>
          <input type="text" name="q" class="navbar-form-search-box" placeholder="Pencarian...">
        </form>
      </div><!-- /.nav navbar-nav navbar-search -->
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown dropdown-notifications">
          <a href="#notifications-panel" class="dropdown-toggle notification-badge" type="button" data-toggle="dropdown">
            <div class="notification-bell">
              <i class="fa fa-bell"></i>
              <span class="badge badge-notify">20</span>
            </div>
          </a>

          <div class="dropdown-container">

            <div class="dropdown-toolbar">
              <div class="dropdown-toolbar-actions">

              </div>
              <h3 class="dropdown-toolbar-title">Pemberitahuan (20)</h3>
            </div><!-- /dropdown-toolbar -->

            <ul class="dropdown-menu">
              <li class="notification">
                <div class="media">
                  <div class="media-left">
                    <div class="media-object">
                      <img data-src="holder.js/50x50?bg=cccccc" class="img-circle" alt="50x50" src="images/notif-icon-dummy-red.png" data-holder-rendered="true" >
                    </div>
                  </div>
                  <div class="media-body">
                    <p class="notification-title">Peringatan: Waktu <a href="#">Ujian Bahasa Indonesia</a> untuk <a href="#">Kelas XII-IPA-1</a> akan dimulai 2 jam lagi. Mohon mempersiapkan segala persiapan ujian</p>

                    <div class="notification-meta">
                      <small class="timestamp">Hari ini jam 12:00</small>
                    </div>
                  </div>
                </div>
              </li>
              <li class="notification active">
                <div class="media">
                  <div class="media-left">
                    <div class="media-object">
                      <img data-src="holder.js/50x50?bg=cccccc" class="img-circle" alt="50x50" src="images/notif-icon-dummy-green.png" data-holder-rendered="true" >
                    </div>
                  </div>
                  <div class="media-body">
                    <p class="notification-title">Informasi: Waktu <a href="#">Ujian Bahasa Indonesia</a> untuk <a href="#">Kelas XII-IPA-1</a> akan dimulai besok hari</p>

                    <div class="notification-meta">
                      <small class="timestamp">Hari ini jam 12:00</small>
                    </div>
                  </div>
                </div>
              </li>
              <li class="notification active">
                <div class="media">
                  <div class="media-left">
                    <div class="media-object">
                      <img data-src="holder.js/50x50?bg=cccccc" class="img-circle" alt="50x50" src="images/notif-icon-dummy-green.png" data-holder-rendered="true" >
                    </div>
                  </div>
                  <div class="media-body">
                    <p class="notification-title">Informasi: Waktu <a href="#">Ujian Bahasa Indonesia</a> untuk <a href="#">Kelas XII-IPA-1</a> akan dimulai besok hari</p>

                    <div class="notification-meta">
                      <small class="timestamp">Hari ini jam 12:00</small>
                    </div>
                  </div>
                </div>
              </li>
              <li class="notification">
                <div class="media">
                  <div class="media-left">
                    <div class="media-object">
                      <img data-src="holder.js/50x50?bg=cccccc" class="img-circle" alt="50x50" src="images/notif-icon-dummy-green.png" data-holder-rendered="true" >
                    </div>
                  </div>
                  <div class="media-body">
                    <p class="notification-title">Informasi: Waktu <a href="#">Ujian Bahasa Indonesia</a> untuk <a href="#">Kelas XII-IPA-1</a> akan dimulai besok hari</p>

                    <div class="notification-meta">
                      <small class="timestamp">Hari ini jam 12:00</small>
                    </div>
                  </div>
                </div>
              </li>

            </ul>

            <div class="dropdown-footer text-center">
              <a href="#">Lihat Semua Pemberitahuan</a>
            </div><!-- /dropdown-footer -->

          </div><!-- /dropdown-container -->
        </li>
        <li>
          <a href="login.php" class="logout-button">
            <button type="button" class="btn btn-pn-red btn-pn-round btn-md" name="button"><i class="fa fa-power-off"></i> <span class="hidden-xs hidden-sm">KELUAR</span></button>
          </a>
        </li>
      </ul>
  </div>
</nav>
