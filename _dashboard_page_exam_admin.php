<div class="container-fluid">
  <div class="row">
    <?php include '_breadcrumb_exam.php'; ?>
    <div class="col-lg-12">
        <h3>Ujian
          <small class="hidden-xs">Daftar Ujian</small>
          <div class="pull-right">
            <a href="dashboard.php?page=exam-add" class="btn btn-sm btn-pn-primary btn-pn-round">
              <span class="hidden-sm hidden-xs"><i class="fa fa-plus-circle"></i> TAMBAH UJIAN</span>
              <span class="hidden-md hidden-lg"><i class="fa fa-plus-circle"></i></span>
            </a>
          </div>
        </h3>
        <div class="row">
          <div class="col-md-12">
            <div class="col-card">
              <div class="table-responsive">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>No.</th>
                      <th>Nama Ujian</th>
                      <th>Status Ujian</th>
                      <th>Selesai</th>
                      <th>Belum Selesai</th>
                      <th>Nama Pelajaran</th>
                      <th>Nama Kelas</th>
                      <th>Guru</th>
                      <th width="8%">Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php include '_table_exam_admin_list.php'; ?>
                    <?php include '_table_exam_admin_list.php'; ?>
                    <?php include '_table_exam_admin_list.php'; ?>
                    <?php include '_table_exam_admin_list.php'; ?>
                    <?php include '_table_exam_admin_list.php'; ?>
                    <?php include '_table_exam_admin_list.php'; ?>
                  </tbody>
                </table>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript" src="libraries/canvasjs-1.8.0/canvasjs.min.js"></script>
<script type="text/javascript" src="scripts/pie_chart.js"></script>
