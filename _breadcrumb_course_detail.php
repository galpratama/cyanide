<div class="col-md-12">
  <div id="bc1" class="btn-group btn-breadcrumb">
    <a href="dashboard.php?page=home" class="btn btn-default"><i class="fa fa-home"></i> Beranda</a>
    <a href="#" class="btn btn-default"><div>Pelajaran</div></a>
    <a href="dashboard.php?page=course" class="btn btn-default"><div>Matematika</div></a>
    <a href="#" class="btn btn-success"><div>XII-IPA-1</div></a>
  </div>
  <div class="pull-right">
    <a href="dashboard.php?page=course" class="btn btn-danger">
      <span class="hidden-xs"><i class="fa fa-chevron-left"></i> Kembali ke Pelajaran "Matematika"</span>
      <span class="visible-xs"><i class="fa fa-times"></i></span>
    </a>
  </div>
</div><!-- /.col-md-12 -->
