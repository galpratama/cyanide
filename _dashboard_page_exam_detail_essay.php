<div class="container-fluid">
  <div class="row">
    <?php include '_breadcrumb_exam_detail_essay.php'; ?>
    <div class="col-lg-12">
      <h2>Ujian Essay 1 Matematika</h2>
      <div class="row">
        <div class="col-md-8">
          <h4>Grafik Sebaran Nilai</h4>
          <div class="col-card">
            <div class="chart-pie">
              <div id="chartContainer"
                   style="height: 323px; width: 100%;">
              </div>
              <br />
              <div class="text-center">Sebaran Nilai</div><!-- /.text-center -->
            </div>
          </div><!-- /.col-card -->
        </div><!-- /.col-md-8 -->
        <div class="col-md-4">
          <h4>Informasi dan Status</h4>
          <?php include '_card_exam_information.php'; ?>
        </div><!-- /.col-md-4 -->
      </div><!-- /.row -->
      <div class="row">
        <div class="col-md-12">
          <h4>Detail Ujian Essay</h4>
          <div class="col-card card-floating-button">
            <div>
              <!-- Nav tabs -->
              <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active">
                  <a href="#tabSoal" aria-controls="tabSoal" role="tab" data-toggle="tab">
                    <i class="fa fa-list"></i> Soal Essay
                  </a>
                </li>
                <li role="presentation">
                  <a href="#tabAnalisis" aria-controls="tabAnalisis" role="tab" data-toggle="tab"><i class="fa fa-chart"></i>
                    <i class="fa fa-line-chart"></i> Analisis Essay
                  </a>
                </li>
              </ul>

              <!-- Tab panes -->
              <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="tabSoal">
                  <div class="button-floating-right-top">
                    <a href="dashboard.php?page=add-soal-essay" class="btn btn-pn-primary btn-sm">
                      <i class="fa fa-plus-circle"></i> Tambah Soal
                    </a>
                    <a  href="#" data-toggle="modal" data-target="#modalBankSoal" class="btn btn-primary btn-sm">
                      <i class="fa fa-file-text-o"></i> Bank Soal
                    </a>
                  </div>
                  <div class="exam-list-table">
                    <?php include '_table_exam_essay.php'; ?>
                  </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="tabAnalisis">
                  <div class="button-floating-right-top">
                    <a href="#" class="btn btn-primary btn-sm">
                      <i class="fa fa-download"></i> Unduh ke Berkas Excel
                    </a>
                  </div>
                  <div class="tab-pane-analysis">
                    <div class="row text-center">
                      <div class="col-md-3 col-sm-3">
                        <span class="text-point"><i class="fa fa-arrow-circle-up"></i> Nilai Tertinggi:</span>
                        <span class="text-green text-score">90</span>
                      </div>
                      <div class="col-md-3 col-sm-3">
                        <span class="text-point"><i class="fa fa-arrow-circle-down"></i> Nilai Terendah:</span>
                        <span class="text-green text-score">70</span>
                      </div>
                      <div class="col-md-3 col-sm-3">
                        <span class="text-point"><i class="fa fa-info-circle"></i> Rata-Rata:</span>
                        <span class="text-green text-score">87</span>
                      </div>
                      <div class="col-md-3 col-sm-3">
                        <span class="text-point"><i class="fa fa-info-circle"></i> Standar Deviasi:</span>
                        <span class="text-green text-score">83</span>
                      </div>
                    </div><!-- /.row -->
                    <div class="analysis-table">
                      <?php include '_table_exam_analysis_essay.php'; ?>
                    </div><!-- /.analysis-table -->
                  </div><!-- /.tab-pane-content -->
                </div>
                <div class="visible-xs">
                  <strong>Informasi:</strong>
                  <br>
                  <i>Untuk anda yang mengakses lewat handphone/smartphone, silahkan geser tabel ke kiri
                  untuk melihat data selengkapnya</i>
                </div>
              </div>
            </div>
          </div>

          </div><!-- /.col-card -->
        </div><!-- /.col-md-12 -->
      </div><!-- /.row -->
    </div><!-- /.col-md-12 -->
  </div><!-- /.row -->
</div><!-- /.container-fluid -->
<script type="text/javascript" src="libraries/canvasjs-1.8.0/canvasjs.min.js"></script>
<script type="text/javascript" src="scripts/column_chart.js"></script>
