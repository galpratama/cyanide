<?php
// How to use
// To show modal loading, add attributes "show" after "modal-loading"
// Example :
// <div class="modal-loading show"></div>
?>

<div class="modal-loading"></div>
